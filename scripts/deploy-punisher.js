// Description:
//    Deploys the Bad Janet
//
// Commands:
//    hubot deploy punisher

const dir = '/srv/deploy'

/**
 * hubot deploy punisher
 */
module.exports = function(robot) {
  robot.respond('/deploy punisher/i', function(msg) {
    msg.send('Deploying Punisher bot!');
    // pull
    execPromise('sudo -u deploy /srv/deploy/deploy_punisher.sh')
      // then post results
      .then(result => {
        msg.send(result);
        msg.send('Punisher has been deployed.');
        return true;
      })
      .catch(err => {
        msg.send(err);
      });
  });
};

function execPromise(cmd) {
  const exec = require('child_process').exec;
  return new Promise((resolve, reject) => {
    exec(cmd, {cwd: dir}, function (error, stdout, stderr) {
      if (error) {
        console.warn(stderr);
        return reject(stderr);
      } else {
        return resolve(stdout);
      }
    });
  });
}
