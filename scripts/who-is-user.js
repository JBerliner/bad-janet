// Description:
//    Who is user?
//
// Dependencies:
//    None
//
// Configuration:
//    None
//
// Commands:
//    hubot who is @user?

module.exports = function(robot) {
  robot.respond(/who is @.+\??\s?$/gi, function(msg) {
    let input_message = msg.match[0];
    let name = input_message.substring(input_message.lastIndexOf('@') + 1);
    let user_name = name.replace(/\s?\??$/, '');
    user_name = user_name.trim();
    // WHAT IS THIS
    user_name = user_name.replace(/\u200B/, '');
    console.log(user_name);
    // Try to find the user in the brain first
    let user = robot.brain.userForName(user_name);

    if (user) {
      msg.send(`I remember <@${user.id}>.
id: ${user.id}
name: ${user.name}
handle: ${user.nickname}
discriminator: ${user.discriminator}`);
    } else {
      msg.send(`Who? What? Who's talking? I don't know this '${user_name}' user you're talking about.`);
    }
  });
}
