// Description:
//    Tops can add additional consequences for messing up the count
//
// Commands:
//    hubot add/set {one-time} count msg  @user text
//    hubot remove count msg {id}
//    hubot see count msg
//

const helpers = require('../src/helpers.js');
const consent = require('../src/consent.js');
const admin_room = process.env.ADMIN_ROOM;
const MessageStacker = require('../src/classes/MessageStacker.js');

/**
 * help count message
 */
module.exports = function(robot) {
  robot.hear(/^(help\s?)?count(ing)?\s?(message|msg)s?\??$/i, function(msg) {
    robot.discordJsMessage.reply(`**Counting Messages (Punishments)**
Add some text that will display with the "fruiting" message. If more than one is added, something random will display. The original purpose of this is to add additional punishments when a user messes up the count, but it could be used for other messages as well such as encouragement, beratement, etc.

*Requires consent to add messages.*

:small_blue_diamond:\`@Bad Janet add count msg @user text message\` Add a message. If there's more than one added, the bot will choose one randomly

:small_blue_diamond:\`@Bad Janet add onetime count msg @user text message\` Add a one-time message. OT Message will be removed after being given.

:small_blue_diamond:\`@Bad Janet override count msg @user text message\` Set one message. Will override any others you had set, so if you had 3 added, and use \`override\` you will only have this one remaining. Does not remove or override messages set by other users.

:small_blue_diamond:\`@Bad Janet override onetime count msg @user text message\` same as above except it's a one-time message that will be removed after being given

:small_blue_diamond:\`@Bad Janet see count msg @user\` User optional; see either all you've set for a user, or all you've set for all users. Will provide IDs for removing messages from the list. Users cannot look up what messages have been set for them.

:small_blue_diamond:\`@Bad Janet remove count msg @user\` Remove all messages you've set for a user

:small_blue_diamond:\`@Bad Janet remove count msg {id}\` Remove a specific message you've set for a user

Note that you can use "count" or "counting" and "message" or "msg" and spaces are optional. For example \`@Bad Janet add counting message\` works and so does \`@Bad Janet addCountMsg\`.
To add surprise messages without your sub knowing, DM the command to Bad Janet, just leave off the @Bad Janet part (you will need to form the @tag using \`<@user_id>\` for this to work; ask for help if you don't know how).`);
  })

  /**
   * count message audit
   */
  robot.hear(/^count(ing)?\s?(message|msg)s?\s?audit$/i, function(msg) {
    if (admin_room && admin_room != msg.message.room) {
      msg.send("You can't to that here.");
    } else {
      let reply = 'Messages:';
      let messages = helpers.brainKeyExists('count_fail_msgs');
      let this_message = '';
      let temp_reply = '';
      for (let user_id in messages) {
        for (let key in messages[user_id]) {
          this_message = `
id: ${messages[user_id][key].id}
is_one_time: ${messages[user_id][key].is_one_time}
text: ${messages[user_id][key].text}
date_set: ${messages[user_id][key].date_set}
set_by: <@${messages[user_id][key].set_by}>
given_to: <@${messages[user_id][key].given_to}>
`;
          temp_reply = reply + this_message
          if (temp_reply.length > 2000) {
            msg.send(reply);
            reply = this_message;
          } else {
            reply = temp_reply
          }
        }
      }
      msg.send(reply);
    }
  });

  /**
   * delete count message (id)
   */
  robot.hear(/^(remove|delete)\s?count(ing)?\s?(message|msg)/i, function(msg) {
    if (admin_room && admin_room != msg.message.room) {
      msg.send("You can't to that here.");
    } else {
      let id_match = msg.message.match(/\d+/);
      let id = id_match[0];
      let messages = helpers.brainKeyExists('count_fail_msgs');
      for (let user_id in messages) {
        for (let key in messages[user_id]) {
          if (key == id) {
            delete robot.brain.data.count_fail_msgs[user_id][id];
            msg.send(id + ' deleted');
            return;
          }
        }
      }
      msg.send('Not found; nothing deleted.');
    }
  });

  /**
   * add/override count message (id)
   */
  robot.respond(/(add|override)\s?(one[-\s]?time )?count(ing)?\s?(message|msg) @/i, function(msg) {
    // Get @user from mentions
    let raw_message = robot.discordJsMessage;
    let mentioned_user_id = helpers.getFirstUserIdFromMentionOnRespond(raw_message);

    let sender = raw_message.author; // User object

    // Check consent
    if (consent.hasConsentByID(sender.id, mentioned_user_id)) {
      // add to brain
      let id = (new Date()).getTime();

      // Get the message text
      let match = raw_message.content.match(/(message|msg) <@\d+> .*/is);
      let text = match[0].replace(/(message|msg) <@\d+> /is, '');
      // Is one time?
      let is_one_time = raw_message.content.match(/one[-\s]?time/i);
      // Create brain object if needed and get brain object
      let messages = helpers.brainKeyExists('count_fail_msgs', mentioned_user_id);

      // Save regex except it only matches on "set"
      let overwrite = raw_message.content.match(/override\s?(one[-\s]?time )?count(ing)?\s?(message|msg)/i);
      if (overwrite) {
        // If using "set" instead of "add" overwrite all other messages this top set
        clearUserMessages(mentioned_user_id, sender.id);
      }

      // Set brain object information
      messages[id] = {
        'id': id,
        'is_one_time': (!!is_one_time),
        'text': text,
        'date_set': new Date(),
        'set_by': sender.id,
        'given_to': mentioned_user_id,
        'given_count': 0,
        'dm_only': false // TODO: allow for DM messages
      }
      // Save!
      robot.brain.save();
      raw_message.react('✅');
      // How many messages are set?
      let count = countUserMessages(mentioned_user_id);

      let reply_text = "I've added the counting message.\n" +
        `<@${mentioned_user_id}> currently has ${count} counting message(s) set.`;
      if (count > 1) {
        reply_text += ' One will be chosen randomly if they mess up the count.';
      }

      raw_message.reply(reply_text);
    } else {
      raw_message.react('🚫');
      raw_message.reply('You do not have consent to add counting messages to that user.');
    }
  });

  /**
   * remove count message (id)
   */
  robot.respond(/remove\s?count(ing)?\s?(message|msg)s? /i, function(msg) {
    // Get @user from mentions
    let raw_message = robot.discordJsMessage;
    let mentioned_user_id = helpers.getFirstUserIdFromMentionOnRespond(raw_message);
    let sender = raw_message.author; // User object

    if (mentioned_user_id) {
      helpers.brainKeyExists('count_fail_msgs', mentioned_user_id);
      // A user was given!
      let delete_count = clearUserMessages(mentioned_user_id, sender.id)

      // TODO: if there weren't any, say that.
      if (delete_count > 0) {
        raw_message.reply(`${delete_count} messages cleared for <@${mentioned_user_id}>.`);
      } else {
        raw_message.reply('No messages found to clear.');
      }
    } else {
      // Look for an ID
      let match = raw_message.content.match(/remove\s?count(ing)?\s?(message|msg)s? \d+/is);
      let matched_id = match[0].replace(/remove\s?count(ing)?\s?(message|msg)s? /is, '');
      let message = findMessageById(matched_id);
      if (message) {
        // TODO: also allow admin to remove messages. Maybe only in the admin bot room
        if (sender.id == message.set_by) {
          // Delete message; I don't know why this doesn't work
          delete robot.brain.data.count_fail_msgs[message.given_to][matched_id]; // Works
          robot.brain.save();
          raw_message.react('✅');
        } else {
          raw_message.reply("Wait. You weren't the one who set that message! " +
          `Only <@${message.set_by}> can remove ${matched_id}!`);
        }
      } else {
        raw_message.reply('I couldn\'t find that message.');
      }
    }
  });

  /**
   * see count messages @user
   */
  robot.respond(/see\s?count(ing)?\s?(message|msg)s?/i, function(msg) {
    // Get @user from mentions
    let raw_message = robot.discordJsMessage;
    let mentioned_user_id = helpers.getFirstUserIdFromMentionOnRespond(raw_message);
    let sender = raw_message.author; // User object
    if (mentioned_user_id) {
      // A user was given!
      let messages = helpers.brainKeyExists('count_fail_msgs', mentioned_user_id);
      let prepend = `You have set the following messages on <@${mentioned_user_id}>: \n`;
      const stacker = new MessageStacker(prepend);
      for (let id in messages) {
        if (messages[id].set_by == sender.id) {
          let m = messages[id]
          stacker.appendOrSend('💠' + id + (m.is_one_time ? '(one time)' : '') + ': ' + m.text + '\n');
        }
      }
      stacker.finalize('*You can remove any of these by sending me `@Bad Janet remove counting message id_number`*',
        'You don\'t have any message set for that user at the moment.'
      );
    } else {
      // Need to sort through to find all your messages
      let messages = helpers.brainKeyExists('count_fail_msgs');
      const stacker = new MessageStacker('You have set the following messages: \n');
      for (let sub_id in messages) {
        for (let id in messages[sub_id]) {
          if (sender.id == messages[sub_id][id].set_by) {
            let m = messages[sub_id][id]
            stacker.appendOrSend(`💠<@${sub_id}> (${id})` + (m.is_one_time ? ' (one time)' : '') + ': ' + m.text + '\n');
          }
        }
      }
      stacker.finalize('*You can remove any of these by sending me `@Bad Janet remove counting message id_number`*',
        'You don\'t have any message set at the moment.'
      );
    }
  });
}

function findMessageById(provided_id) {
  let messages = helpers.brainKeyExists('count_fail_msgs');
  for (let sub_id in messages) {
    for (let id in messages[sub_id]) {
      if (id == provided_id) {
        return messages[sub_id][id];
      }
    }
  }
}

function clearUserMessages(sub_id, top_id) {
  let user_messages = helpers.brainKeyExists('count_fail_msgs', sub_id);
  let count = 0;
  for (let id in user_messages) {
    if (user_messages[id].set_by == top_id) {
      count++;
      delete user_messages[id];
    }
  }
  /* global robot */
  robot.brain.save();
  return count;
}

function countUserMessages(user_id) {
  let user_messages = helpers.brainKeyExists('count_fail_msgs', user_id);
  let count = 0;
  for (let id in user_messages) {
    count++;
  }
  return count;
}
