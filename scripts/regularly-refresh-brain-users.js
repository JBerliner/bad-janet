// Description:
//    Looks at all the users in the guild and loads them into memory.
//
// Dependencies:
//    None
//
// Configuration:
//    None
//
// Commands:
//    None

const cron = require('node-cron');
const reload = require('../src/reload_users_in_brain.js');

module.exports = function(robot) {
  // Every hour
  cron.schedule('1 0 * * * *', () => {
    reload.reloadAllUsersInBrain(robot);
  });
}
