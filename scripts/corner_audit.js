// Description:
// audit what's in the brain for the shame role
//
// Commands:
//   corner audit

const admin_room = process.env.ADMIN_ROOM;
const helpers = require('../src/helpers.js');
const MessageStacker = require('../src/classes/MessageStacker.js');

/**
 * corner audit
 */
module.exports = function(robot) {
  robot.hear(/^corner audit/i, async function(msg) {
    if (admin_room && admin_room != msg.message.room) {
      msg.send("You can't to that here.");
    } else {
      let list = helpers.brainKeyExists('count_fail_shame_list');
      const stacker = new MessageStacker('');

      for (let user_id in list) {
        let user = await robot.client.guilds.cache.get(process.env.GUILD_ID).members.fetch(user_id);
        let reply = '';
        reply += `**${user.displayName}**\n`;
        let received = new Date(list[user.id].received);
        reply += `Received: ${received}\n`;
        reply += `Timer (ms): ${list[user.id].timer_ms}\n`;
        let over = new Date(list[user.id].over);
        reply += `Over: ${over}\n`;
        reply += `Source: ${list[user.id].source} \n`;
        reply += `Number: ${list[user.id].number}\n`;
        stacker.appendOrSend(reply);
      }
      stacker.finalize('', 'The corner is empty.');
    }
  });
}
