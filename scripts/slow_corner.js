// Description:
//    Slow users down in the counting room with consent
//
// Commands:
//    ?punish
//    ^slow\s?corner\??$
//    hubot slow\s?corner @
//    hubot slow my corner

const _ = require('underscore')
const helpers = require('../src/helpers.js');
const consent = require('../src/consent.js');
const shame_room = process.env.SHAME_ROOM;
const admin_room = process.env.ADMIN_ROOM;
const MessageStacker = require('../src/classes/MessageStacker.js');
const Slowdown = require('../src/classes/Slowdown.js');

// TODO: remove slowdowns (just set to 0, really, but I want to delete it)
// TODO: see set slowdowns
// TODO: Reset after punishment over?

module.exports = function(robot) {
  /**
  * ?punish
  */
  robot.hear(/^\?punish/, function(msg) {
    const channel = robot.client.channels.cache.get(shame_room);
    if (channel.id == robot.discordJsMessage.channel.id) {
      // Was said in the corner of shame
      let raw_message = robot.discordJsMessage;
      let sender = raw_message.author; // User object
      const slowdownClass = new Slowdown(sender);

      let slowdown = helpers.brainKeyExists('slowdown', sender.id);
      let modifier = slowdownClass.getCountModifierSeconds();

      if (_.isEmpty(slowdown) && modifier == 0) {
        channel.setRateLimitPerUser(0, "User doesn't have any slowdowns set. Reset to 0.");
        msg.send('Congrats, you don\'t have any slowdown set. Still, don\'t go too fast or Punisher might miss some numbers.');
      } else {
        let count = slowdownClass.getSlowRecordCount();
        let s = slowdownClass.getSlowRecordSeconds();
        let top_id = slowdownClass.getSlowRecordSetterId();

        let count_msg = '';
        if (count > 1) {
          count_msg = `\nYou had ${count} slowdowns. The highest was chosen.`;
        }
        if (modifier > 0) {
          s = +s + +modifier
          count_msg = `\n**Oh snap** you messed up the counting! **${modifier} seconds** of your slowdown came from messing up the count. (Use \`slow modifier?\` for more information.)`;
        }
        channel.setRateLimitPerUser(s, `Slow down for ${sender.username} based on their settings`);

        let setter_msg = '';
        if (top_id) {
          if (slowdown[top_id].self) {
            setter_msg = 'You have only yourself to blame.';
          } else {
            setter_msg = `You should thank <@${top_id}> for that.`
          }
        }
        msg.send(`You've been slown down! The room is now set to a ${s} second slowmode. ${setter_msg} ${count_msg}`);
      }
    }
  });

  /**
   * slowdown audit
   */
  robot.hear(/^slowdown audit$/i, async function (msg) {
    if (admin_room && admin_room !== msg.message.room) {
      robot.discordJsMessage.reply("You can't do that here.");
    } else {
      const list = helpers.brainKeyExists('slowdown');
      const stacker = new MessageStacker('');
      for (let slowed_id in list) {
        for (let slow_setter_id in list[slowed_id]) {
          if (list[slowed_id][slow_setter_id].seconds === '0') {
            //remove all 0 records
            stacker.appendOrSend(`<@${slowed_id}>: **${list[slowed_id][slow_setter_id].seconds}s** Set by <@${slow_setter_id}>. REMOVED FOR BEING 0!\n`);
            delete robot.brain.data.slowdown[slowed_id][slow_setter_id];
          } else {
            stacker.appendOrSend(`<@${slowed_id}>: **${list[slowed_id][slow_setter_id].seconds}s** Set by <@${slow_setter_id}>.\n`);
          }
        }
      }
      stacker.finalize();
    }
  });

  /**
   * slowcorner?
   */
  robot.hear(/^slow\s?corner\??$/, function(msg) {
    robot.discordJsMessage.reply('Slow down the corner so they can\'t run through it so fast. '
      + 'Use `@Bad Janet slowcorner @user 3` to slow that user messages down '
      + 'to one every 3 seconds (*requires consent*). Use however many seconds you want! :smiling_imp:\n\n'
      + 'Slow yourself down with `@Bad Janet slow my corner 3` (Change 3 to however many seconds you want)\n\n'
      + 'New changes to the Punisher bot may increase one\'s punishment up to 250, '
      + 'so here is what you can expect a slowdown to do '
      + '(these are MINIMUM times, and could take much longer depending on the user):\n'
      + `:small_blue_diamond:0: however fast discord will let them
:small_blue_diamond:1: 1-5 minutes
:small_blue_diamond:2: 3-9m
:small_blue_diamond:3: 5-13m
:small_blue_diamond:4: 6-17m
:small_blue_diamond:5: 8-21m
:small_blue_diamond:6: 10-25m
:small_blue_diamond:7: 11-30m
:small_blue_diamond:8: 13-34m
:small_blue_diamond:9: 15-38m
:small_blue_diamond:10: 16-42m
:small_blue_diamond:11: 18-45m
:small_blue_diamond:12: 20-50m
:small_blue_diamond:13: 21-54m
:small_blue_diamond:14: 23-58m
:small_blue_diamond:15: 25-63m
etc.
We don't recommend more than maybe a 6 second slow down, but you do you, Boo.

Slowmode goes into effect when a user uses the \`?punish\` command. Two users doing punishments at the same time can mess with each other's slow mode.
`
    );
  });

  /**
   * slowcorner @user (time)
   */
  robot.respond(/slow\s?corner\s+(@|\d+)/i, function(msg) {
    // Get user ID
    let raw_message = robot.discordJsMessage;
    let mentioned_user_id = helpers.getFirstUserIdFromMentionOnRespond(raw_message);
    if (!mentioned_user_id) {
      raw_message.reply('Who? I didn\'t catch who you want me to slow down.');
    } else {
      let sender = raw_message.author; // User object

      // Check consent
      let seconds = getDigits(raw_message); // How many seconds is the message for?
      if (consent.hasConsentByID(sender.id, mentioned_user_id) || (seconds !== false && seconds[0] === '0')) {
        // If there's consent OR the message is 0
        // You should be able to clear your set slowdown even if there's no consent
        if (!seconds) {
          raw_message.reply('Did you forget to tell me how many seconds to slow them for?');
        } else {
          // User it's for
          let user_id = helpers.getFirstUserIdFromMentionOnRespond(raw_message);
          // Get the brain key
          let slowdown = helpers.brainKeyExists('slowdown', user_id, sender.id);
          if (seconds[0] == 0) {
            // If you're clearing the slowdown
            seconds = slowdown.seconds;
            // No matter the message, delete the record.
            // We created it earlier even if it was empty.
            delete robot.brain.data.slowdown[user_id][sender.id];
            robot.brain.save();
            if (seconds) {
              // Seconds existed, delete them
              raw_message.reply(`Your slowdown for <@${user_id}> was ${seconds}s, and has been cleared.`);
            } else {
              // Nothing was set
              raw_message.reply('Your didn\'t have a slowdown set for that user to clear.');
            }
          } else {
            // If you're setting a slowdown
            robot.brain.data.slowdown[user_id][sender.id] = {
              'set_by': sender.id,
              'seconds': seconds[0],
              'self': (user_id == sender.id)
            };
            robot.brain.save();
            raw_message.reply(`A slowdown has been added to <@${user_id}> of ${seconds[0]} seconds for when they have to count in the corner.`);
          }
        }
      } else {
        raw_message.react('🚫');
        raw_message.reply('You do not have consent to rate limit that user.');
      }
    }
  });

  function getDigits(message) {
    let digits = message.content.match(/(>|corner)\s+\d+/);
    if (!digits) {
      return false;
    } else {
      return digits[0].match(/\d+/);
    }
  }

  /**
   * slow my corner (time)
   */
  robot.respond(/slow my corner/i, function(msg) {
    let raw_message = robot.discordJsMessage;
    let digits_1 = raw_message.content.match(/corner \d+/);
    if (!digits_1) {
      raw_message.reply('Did you forget to tell me how many seconds to slow you for?');
    } else {
      let seconds = digits_1[0].match(/\d+/);
      // get user id
      let sender = raw_message.author; // User object

      // add to brain
      helpers.brainKeyExists('slowdown', sender.id, sender.id);
      if (seconds[0] == 0) {
        delete robot.brain.data.slowdown[sender.id][sender.id];
        raw_message.reply('Your slowdown has been removed.');
        robot.brain.save();
      } else {
        robot.brain.data.slowdown[sender.id][sender.id] = {
          'set_by': sender.id,
          'seconds': seconds[0],
          'self': true
        };
        raw_message.reply(`I've added a slowdown to you of ${seconds[0]} for when you count in the corner.`);
      }
    }
  });
}
