// Description:
//   give the corner of shame to a user who asks for it
//
// Commands:
//   hubot shame me
//   hubot shame @user

const shame_role = process.env.SHAME_ROLE;
const shame_room = process.env.SHAME_ROOM;
const consent = require('../src/consent.js');
const helpers = require('../src/helpers.js');
const failed_count_msg = require('../src/failed_count_msg.js');
const { MessageEmbed } = require('discord.js');

module.exports = function(robot) {
  /**
   * shame me
   */
  robot.respond(/shame me hard/i, function(msg) {
    let channel = robot.client.channels.cache.get(msg.message.room);
    let task = failed_count_msg.getRandomMsg(msg.message.user);
    if (!task) return channel.send("You don't have any count messages set.");
    let embed = new MessageEmbed()
      .setColor('#0099ff')
      .setTitle('SHAME ON YOU!')
      .addFields({name: "Here's your task.", value: task.text})
    let content = `Heads up <@${msg.message.user.id}>! You've got a special message from <@${task.set_by}>.`;
    channel.send({content: content, embeds: [embed]});
    shameUser(msg, msg.message.user, 'shame_me');
  });

  robot.respond(/shame me$/i, function(msg) {
    shameUser(msg, msg.message.user, 'shame_me');
  });

  robot.respond(/shame (on )?you/i, function(msg) {
    robot.discordJsMessage.reply("Nice try. Go to the corner for that!");
    shameUser(msg, msg.message.user, 'shame_me');
  });

  robot.respond(/shame @/i, function(msg) {
    sender = msg.message.user;
    raw_message = robot.discordJsMessage;
    content = raw_message.content;
    // Get the front half of the message
    let front_half = content.match(/shame <@\d+>/i);
    if (front_half) {
      mentions = raw_message.mentions;
      mentioned_user = mentions.users.first(2)[1]; //User object

      if (!mentioned_user) {
        // I haven't been able to come up with a way
        // That you can get this far unless you're mentioning the bot
        robot.discordJsMessage.reply("Nice try. Go to the corner for that!");
        shameUser(msg, msg.message.user, 'shame_me');
        raw_message.react('❌');
        return;
      }

      // If mentioned user is the bot, shame user for bot shaming
      if (mentioned_user.bot) {
        raw_message.reply("I don't allow bot-shaming around these parts.");
        shameUser(msg, msg.message.user, 'shame_me');
        raw_message.react('❌');
        return;
      }

      if (consent.hasConsent(sender, mentioned_user)) {
        shameUser(msg, mentioned_user, 'shame_command');
      } else {
        msg.send('You do not have consent to shame that user.');
      }
    }
  });

  robot.hear(/\?transfer /i, function(msg) {
    raw_message = robot.discordJsMessage;
    sender = raw_message.author;
    mentions = raw_message.mentions;
    to_user = mentions.users.first(); //User object

    if (!to_user) {
      raw_message.react('❌');
      raw_message.reply('Who?');
    } else {
      guild = raw_message.guild;
      // First, does the user have the shame role?
      let role = guild.roles.cache.find(role => role.name === shame_role);
      robot.client.guilds.cache.get(process.env.GUILD_ID).members.fetch(sender.id)
      .then(guildMember => {
        if (!guildMember.roles.cache.some(role => role.name === shame_role)) {
          // User does not have the shame role, fail here
          raw_message.react('🚫');
          raw_message.reply("You can't transfer a role you don't have.");
        } else {
          // User has shame role, can transfer, maybe
          // Second, make sure the target isn't already shamed
          robot.client.guilds.cache.get(process.env.GUILD_ID).members.fetch(to_user.id)
          .then(receiver_member => {
            if (receiver_member.roles.cache.some(role => role.name === shame_role)) {
              // The target is already shamed, fail here
              raw_message.react('🚫');
              raw_message.reply("You can't transfer the role to someone who already has it.");
            } else {
              // The target is not already shamed
              // Third, does the target consent to the user?
              if (!consent.hasConsent(sender, receiver_member)) {
                // Target does not consent, stop here
                raw_message.react('🚫');
                raw_message.reply('You do not have consent to transfer shame to that user.');
              } else {
                // Reassign array if it exists, otherwise make a new one
                brain_data = robot.brain.data;
                helpers.brainKeyExists('count_fail_shame_list');
                shame_list = robot.brain.data.count_fail_shame_list[guildMember.id];
                if (shame_list) {
                  robot.brain.data.count_fail_shame_list[receiver_member.id] = shame_list;
                  delete robot.brain.data.count_fail_shame_list[guildMember.id];
                } else {
                  timer = (86400000) // 24 hours in milliseconds
                  brain_data = robot.brain.data;
                  time_now = new Date().getTime();
                  robot.brain.data.count_fail_shame_list[receiver_member.id] = {
                    "number": 0,
                    'received': time_now,
                    'timer_ms': timer,
                    'over': time_now + timer,
                    "source": "transfer"
                  }
                }
                robot.brain.save;

                // Remove role from giving user
                guildMember.roles.remove(role);
                // Add role to the target
                receiver_member.roles.add(role);

                const channel = robot.client.channels.cache.get(shame_room);

                // Send in shame room
                channel.send(`<@${guildMember.id}> has passed on their `
                  + `Corner of Shame to you <@${receiver_member.id}> `
                  + 'as they are either your Dominant or you have agreed to '
                  + 'complete their Corner of Shame punishment on their behalf. '
                  + 'Type `?punish` to get started, enjoy.');
                raw_message.delete();
              }
            }
          }) // End looking up target guild member
        }
      }) // End looking up sender guild member
    }
  });
}

async function shameUser(msg, user, source) {
  let user_id = user.id
  await robot.client.guilds.cache.get(process.env.GUILD_ID).members.fetch(user_id)
  .then(member => {
    let role = member.guild.roles.cache.find(role => role.name === shame_role);
    member.roles.add(role);
    msg.send(`SHAME ON <@${user_id}>!`);

    timer = (86400000) // 24 hours in milliseconds
    brain_data = robot.brain.data;
    time_now = new Date().getTime();
    helpers.brainKeyExists('count_fail_shame_list');
    brain_data['count_fail_shame_list'][user_id] = {
      'number': 0,
      'received': time_now,
      'timer_ms': timer,
      'over': time_now + timer,
      'source': source
    };
    robot.brain.save();
  });
}
