// Description:
//    Listens for when someone messes up the counting game and
//    publicly shames them.
//
// Dependencies:
//    discord.js
//
// Configuration:
//    environment variable FRUITING_ROOM={room_id}
//    environment variable SHAME_ROLE={role text}
//
// Commands:
//    none

const { MessageEmbed } = require('discord.js');
const shame = require('../src/shame.js');
const failed_count_msg = require('../src/failed_count_msg.js');
const helpers = require('../src/helpers.js');
const where_to_fruit = process.env.FRUITING_ROOM;
const counting_bot = '510016054391734273'; // counting bot ID

const titles = [
  'Bring on the fruit',
  'Pass the bananas',
  'Someone\'s going to need to go to the store and buy more fruit.',
  'Time to make some fruit salad on their face.',
  "I'm not mad, I'm just disappointed.",
  "I hope everyone's throwing arm is warmed up",
  "Two comes after one, three comes after two. Come on, it's not that hard!",
  'I think someone needs to go back to school.',
  'Someone is spending some time in the corner to practice their counting.',
  "This is why we can't have nice things.",
  'Oh come on. You can do better than that!',
  "You've dashed all my hopes and dreams.",
  'I believed in you and look at you now.',
];

module.exports = function(robot) {
  robot.hear(/@.*\sRUINED IT AT \*\*\d*\*\*/g, function(msg) {
    if (!testViability()) {
      return;
    }
    let raw_message = robot.discordJsMessage;
    let mentions = raw_message.mentions;
    let user = mentions.users.first(); // User object
    let ruined_it_at = msg.match[0];
    let array = ruined_it_at.split('RUINED IT AT');
    let bolded_number = array[1].trim();

    msg.send(`SHAME ON <@${user.id}>! Head over to <#928783575627751505> to practice your counting and remove your new Corner of Shame role.\n`
      + 'If you do nothing your role of shame will fall off on its own... eventually. :smiling_imp:'
    );
    let description = `**<@${user.id}>** messed up the counting game at ${bolded_number}!`;
    shameForBadCounting(msg, user, bolded_number, description);
  });

  robot.hear(/@.* You have used \*\*\d*\*\* guild save!/gi, function(msg) {
    if (!testViability()) {
      return;
    }
    let raw_message = robot.discordJsMessage;
    let mentions = raw_message.mentions;
    let user = mentions.users.first(); // User object

    msg.send(`SHAME ON <@${user.id}>! Why? Because you messed up the count!! `
      + 'Users voted and donated their saves *just so you could mess up*; the least you can do is count your way out of the corner. \n'
      + 'Head over to <#928783575627751505> to practice your counting and remove your new Corner of Shame role.\n'
      + 'If you do nothing your role of shame will fall off on its own... eventually. :smiling_imp:'
    );
    let description = `**<@${user.id}>** messed up counting and used up a guild save!`;
    let next_number = robot.discordJsMessage.content.match(/number is \*\*\d*\*\*/ig);
    let number_match = next_number[0].split('number is ');
    let bolded_number = number_match[1].trim();
    shameForBadCounting(msg, user, bolded_number, description);
  })

  robot.hear(/@.* You have used \*\*\d*\*\* of your saves/gi, function(msg) {
    if (!testViability()) {
      return; // stop
    }
    let raw_message = robot.discordJsMessage;
    let mentions = raw_message.mentions;
    let user = mentions.users.first(); // User object

    msg.send(`SHAME ON <@${user.id}>! Why? Because you messed up the count!! `
      + '***Counting-bot saves can\'t save you from your SHAME.*** \n'
      + 'Head over to <#928783575627751505> to practice your counting and remove your new Corner of Shame role.\n'
      + 'If you do nothing your role of shame will fall off on its own... eventually. :smiling_imp:'
    );
    let description = `**<@${user.id}>** messed up counting and used up one of their saves!`;
    let next_number = robot.discordJsMessage.content.match(/number is \*\*\d*\*\*/ig);
    let number_match = next_number[0].split('number is ');
    let bolded_number = number_match[1].trim();
    shameForBadCounting(msg, user, bolded_number, description);
  })

  function testViability() {
    if (!where_to_fruit) {
      robot.logger.error("There's nowhere to throw any fruit.");
      return 0;
    }
    if (robot.discordJsMessage.author.id != counting_bot) {
      robot.logger.error(`OH snap, user ${robot.discordJsMessage.author.id} attempted to trigger these commands! D=`);
      return 0;
    }
    return 1;
  }

  function shameForBadCounting(msg, user, bolded_number, description) {
    // Track counting failures
    let count = helpers.brainKeyExists('count_fail_tracking');
    if (!count[user.id]) {
      count[user.id] = 0;
    }
    count[user.id]++;

    let extra_message = failed_count_msg.getRandomMsg(user);

    const exampleEmbed = new MessageEmbed()
      .setColor('#0099ff')
      .setTitle(msg.random(titles))
      .setURL('https://discord.com/channels/' + process.env.GUILD_ID + '/' + msg.message.room + '/' + msg.message.id)
      .setDescription(description)
      .setImage(user.avatarURL())
      .setTimestamp()
      ;

    const channel = robot.client.channels.cache.get(where_to_fruit);

    if (extra_message) {
      exampleEmbed.addFields(
        { name: '\u200B', value: '\u200B' },
        {name: 'Special Message:', value: extra_message.text},
        { name: '\u200B', value: '\u200B' },
      )
      let content = `Heads up <@${user.id}>! You've got a special message from <@${extra_message.set_by}>.`;
      channel.send({content: content, embeds: [exampleEmbed] })
        .then((message) => {
          message.react('🍅'); // Tomato
          message.react('🍑'); // Peach
          message.react('🥝'); // Kiwi
          message.react('🫐'); // Blueberries
          message.react('🍌'); // Banana
          message.react('🥭'); // Mango
          message.react('🍒'); // Cherries
          message.react('🍓'); // Strawberry
          message.react('🍇'); // Grapes
        });
      // Remove one-time messages from the brain
      if (extra_message.is_one_time) {
        delete robot.brain.data.count_fail_msgs[user.id][extra_message.id];
        robot.brain.save();
      }
    } else {
      channel.send({embeds: [exampleEmbed] })
        .then((message) => {
          message.react('🍅'); // Tomato
          message.react('🍑'); // Peach
          message.react('🥝'); // Kiwi
          message.react('🫐'); // Blueberries
          message.react('🍌'); // Banana
          message.react('🥭'); // Mango
          message.react('🍒'); // Cherries
          message.react('🍓'); // Strawberry
          message.react('🍇'); // Grapes
        });
    }

    robot.client.guilds.cache.get(process.env.GUILD_ID).members.fetch(user.id)
      .then(user => {
        // Add shame role
        shame.addShameByUserId(user);

        // Save shame timer and data
        const max = 3600000; // 1 hour in milliseconds
        const min = 18000000; // 5 hours in milliseconds
        let timer = Math.floor(Math.random() * (max - min + 1) + min);
        let number = bolded_number.replaceAll('*', '');
        shame.shameUserRecord(user, number, timer, 'counting');
      });
  }
}
