// Description:
//    respond to help
//
// Commands:
//    hubot help

module.exports = function(robot) {
  robot.respond(/help/i, function(msg) {
    robot.discordJsMessage.reply("I'm not sure anyone can help you. I know I can't.");
  });

  robot.hear(/^orientation/i, function(msg) {
    robot.discordJsMessage.reply(`Welcome to the WFM community Discord server!
Here are some things you might want to know about us!

We're friendly but can be quiet. The server is small so goes through periods of a lot of chatter to dead silence, but someone is usually around if you start talking. We have members from all over the world and it usually means that no matter the time, a portion of our server is asleep or awake.

**SFW / NSFW Discussion**
Keep <#928042507152007208> to safe for work topics, pictures, etc., if you want to get kinky, take it to <#956334375396581406>. Most rooms are SFW since we are mostly about discussion, but we have a few NSFW channels. Check the icon next to the room or just ask if you wanted to post something NSFW somewhere and you're not sure. <#956334375396581406> is *almost* anything goes, as long as it's within our rules, so it's always safe to post there when you're unsure. You can see more info on our rooms in the <#998399223185805362>.

**Counting / Server Games**
We love to count and tease each other. Check out the counting game in #counting but make sure you read the information here before you join in: https://discord.com/channels/928037167677181952/928658406921883658/998994639640084655

If you mess up the count, the consequences are usually a pink "Corner of Shame" role which you can remove via counting your way out in the <#928783575627751505>. However, beta and some others are setting messages on people who give them consent. If you're brave, run:
> <@929783745513406504> consent all
 and let people have some fun. See this post for more information on consent: https://discord.com/channels/928037167677181952/928298290783600690/997994922378534984

**Bot Commands**
Make sure you check out <#928298290783600690> to see all the things you can do with our bots. We custom code the bots and if you have any ideas you'd like to see happen, we might be able to make it work.
`);

robot.discordJsMessage.reply(`**Events**
We are doing streaming events in our voice channel. If you'd like to be alerted when we run events, read and react to this message: https://discord.com/channels/928037167677181952/928037167677181955/1016863965315338251

Would you like to run an event??? Events you can run include:
* Streaming your browser while typing on WFM
* Streaming yourself handwriting lines
* Organize a handwritten lines contest
* Organize a WFM lines contest
* Stream a play session
* Stream a movie night with the server
* Whatever you can come up with

You can do most of the streaming things any time, but we will announce them as an event if you schedule it ahead of time. 

`);
  });
}
