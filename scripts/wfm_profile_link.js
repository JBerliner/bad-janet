// Description:
//    Track user WFM profiles
//
// Commands:
//    set wfm
//    wfm @user
//    wfm?
//    wfm profile?


const helpers = require('../src/helpers.js');
const MessageStacker = require('../src/classes/MessageStacker.js');

module.exports = function(robot) {
  /**
   * wfm?
   */
  robot.hear(/^wfm\??$/i, function(msg) {
    msg.send('I remember WFM profiles! \nYou can set your profile using '
      + '`set wfm LINK HERE` \n'
      + 'You can retrieve anyone\'s set profile with `wfm @user`\n'
      + 'Ask `wfm profile?` if you need help finding your profile link in WFM.\n'
      + 'If you ever want to remove your profile link you can use `set wfm` without any link and it will erase your entry'
    );
  });

  /**
   * wfm profile?
   */
  robot.hear(/^wfm profile\??$/i, function(msg) {
    msg.send(`You can't just go to "show my profile" to get your profile link in WFM. Here are some ways you can find your profile link:
  * Use the "User Search" in the left hamburger menu of the site.
  * Make a task and go to it. Click on your name.
  * Complete a task and find your name on the list of people who have completed it and click on it.
  * Make a post somewhere in the forums and click on your name on your post.

Ask if you need more help. =)`
    );
  });

  /**
   * set wfm (link)
   */
  robot.hear(/^set wfm\s?/i, function(msg) {
    helpers.brainKeyExists('wfm_profiles');
    let raw_message = robot.discordJsMessage;
    if (raw_message.content.replace(/^set wfm\s?/i, '') == '') {
      delete robot.brain.data.wfm_profiles[raw_message.author.id];
      raw_message.reply('I\'ve removed your profile link.');
    } else {
      let link = raw_message.content.replace(/^set wfm /i, '');
      if (link.match(/LINK/i)) {
        raw_message.reply('You need to replace `LINK` with your WFM profile link. Use `wfm profile` if you need help finding your link.');
      } else if (link == 'https://writeforme.org/profile/') {
        raw_message.reply('I\'m sorry that\'s the page everyone views their own profiles on. Use `wfm profile` if you need help finding your link.');
      } else if (link.match(/@everyone/i) || link.match(/@here/i)) {
        // test for bullshit
        raw_message.reply('fuck you');
      } else {
        link = link.trim();
        robot.brain.data.wfm_profiles[raw_message.author.id] = link;
        robot.brain.save();
        raw_message.reply('I will remember your profile is '
        + link
        + '\nAnyone can call it up using "wfm <@'
        + raw_message.author.id
        + '>"');
      }
    }
  });

  /**
   * wfm @user
   */
  robot.hear(/^wfm @/i, function(msg) {
    let raw_message = robot.discordJsMessage;
    let mentioned_user_id = helpers.getFirstUserIdFromMentionOnHear(raw_message);
    let list = helpers.brainKeyExists('wfm_profiles');
    if (list[mentioned_user_id]) {
      raw_message.reply(list[mentioned_user_id]);
    } else {
      raw_message.reply('No profile set.');
    }
  });

  /**
   * wfm dir / directory
   */
  robot.hear(/^wfm dir(ectory)?\??$/i, async function(msg) {
    let list = helpers.brainKeyExists('wfm_profiles');
    const stacker = new MessageStacker('');

    for (let user_id in list) {
      try {
        let guildMember = await robot.client.guilds.cache.get(process.env.GUILD_ID).members.fetch(user_id);
        stacker.appendOrSend(`${guildMember.displayName}: ${list[user_id]}\n`);
      } catch (error) {
        // I don't feel like showing unknown members anymore
        //stacker.appendOrSend(`Unknown member: ${list[user_id]}\n`);
      }
    }
    stacker.finalize('', 'Directory is empty.');
  });

  /**
   * wfm cleanup
   * Clean up URL mistakes that happened when users did things the code didn't trap for
   */
  robot.hear(/^wfm cleanup/i, async function(msg) {
    msg.send('Cleaning up wfm links...');
    let list = helpers.brainKeyExists('wfm_profiles');

    for (let user_id in list) {
      let url = list[user_id];
      if (url.match(/link/i)) {
        url = url.replace(/\s?link\s?\n?\s?/i, '');
        robot.brain.data.wfm_profiles[user_id] = url;
        robot.brain.save();
      }
      if (url.match(/Set wfm /i)) {
        url = url.replace(/Set wfm /i, '');
        robot.brain.data.wfm_profiles[user_id] = url;
        robot.brain.save();
      }
      if (url.match(/\s/)) {
        url = url.replace(/\s/, '');
        robot.brain.data.wfm_profiles[user_id] = url;
        robot.brain.save();
      }
    }
  });
}
