// Description:
//    Warn users about bad counting
//
// Commands:
//    None

const counting_room = process.env.COUNTING_ROOM;

// Regex -
// ^ Match on the start of a message
// \d+[\#\d]*\.?[\#\d]*
//    any number of digits,
//    # or a digit, 0 or more times
//    ., 0 or one time
//    # or a digit, 0 or more times
//    3#####.####### is valid
//    3#####.####4## is valid
//    3#####.####4##5 is valid
// (\s+|$) Either zero or more space, or end of line

// Const { evaluate } = require('mathjs')

// limit the evaluation per mathjs docs
// https://mathjs.org/examples/advanced/more_secure_eval.js.html
const { create, all } = require('mathjs')
const math = create(all)

const limitedEvaluate = math.evaluate

math.import({
  import: function () { throw new Error('Function import is disabled') },
  createUnit: function () { throw new Error('Function createUnit is disabled') },
  evaluate: function () { throw new Error('Function evaluate is disabled') },
  parse: function () { throw new Error('Function parse is disabled') },
  simplify: function () { throw new Error('Function simplify is disabled') },
  derivative: function () { throw new Error('Function derivative is disabled') }
}, { override: true })


// Matches on all messages
module.exports = function(robot) {
  robot.client.on('messageCreate', message => {
    // only in counting room
    if (message.channelId == counting_room // counting room
      && !message.author.bot // not a bot
      // This seems to be the regex for counting bot
      && !message.content.match(/^\d+[#\d]*\.?[#\d]*(\s+|$)/) // Digits and either EOL or space
    ) {
      // This isn't a straight count message
      // But apparently, things like 1+1 count

      // First, does it start with either a ( or a digit?
      if (message.content.match(/^[\d(]/) && message.content.match(/\d+/)) {
        try {
          // Try evaluating the string as-is
          let to_evaluate = message.content.match(/^.*(\s|$)/);
          let e = limitedEvaluate(to_evaluate[0])
          if (!e) {
            // Still didn't evaluate
            reactAppropriately(message, '🤔', 'I couldn\'t react! I couldn\'t evaluate the string.');
            reactAppropriately(message, '❓');
          } else {
            reactAppropriately(message, '#️⃣', "I couldn't react! String mathmatically evaluated.");
          }
        } catch {
          // Did not evaluate as is.
          // Is there a space?
          if (message.content.match(/\s/)) {
            let first_part = message.content.match(/.*\s/);
            try {
              let e = limitedEvaluate(first_part[0]);
              if (!e) {
                // Still didn't evaluate
                reactAppropriately(message, '🤔', 'I couldn\'t react! I couldn\'t evaluate the string.');
                reactAppropriately(message, '❓');
              } else {
                reactAppropriately(message, '#️⃣', "I couldn't react! String mathmatically evaluated.");
              }
            } catch {
              // Still didn't evaluate
              reactAppropriately(message, '🤔', 'I couldn\'t react! I couldn\'t evaluate the string.');
              reactAppropriately(message, '❓');
            }
          } else {
            reactAppropriately(message, '❌', 'I couldn\'t react! Warning! Be careful about this message!');
            reactAppropriately(message, '⚠️');
          }
        }
      } else {
        // Doesn't start with ( or a digit or it has no digits
        reactAppropriately(message, '❌', 'I couldn\'t react! Warning! Be careful about this message!');
        reactAppropriately(message, '⚠️');
      }
    } else if (message.channelId == counting_room // counting room
      && !message.author.bot // not a bot
      && message.reference
    ) {
      // This is a reply
      reactAppropriately(message, '⭕', 'I couldn\'t react! Warning! This was a reply and not considered by the bot.');
      reactAppropriately(message, '⚠️');
    }
  });

  robot.client.on('messageUpdate', (oldMessage, newMessage) => {
    // Only in counting room
    if (newMessage.channelId == counting_room // counting room
      && !newMessage.author.bot // not a bot USE NEW MESSAGE
      && oldMessage.content // old message has to have content (not cached?)
    ) {
      if (
        (oldMessage.content.match(/^[\d(]/) && oldMessage.content.match(/\d+/))
        || (newMessage.content.match(/^[\d(]/) && newMessage.content.match(/\d+/))
      ) {
        newMessage.react('✏️');
        newMessage.react('⚠️');
        newMessage.reply('Message edited. The old message was **' + oldMessage.content + '**, changed to ' + newMessage.content);
      }
    }
  });

  async function reactAppropriately(message, react, message_on_err) {
    try {
      await message.react(react)
    } catch (error) {
      if (message_on_err) {
        message.reply(message_on_err);
      }
      console.log(error);
    }
  }
}
