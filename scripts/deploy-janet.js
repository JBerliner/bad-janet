// Description:
//    Deploys the Bad Janet
//
// Commands:
//    hubot deploy yourself

const dir = '/srv/deploy'

/**
 * hubot deploy yourself
 */
module.exports = function(robot) {
  robot.respond('/deploy yourself/i', function(msg) {
    msg.send("I can do that but if I don't come back you aren't going to know why because I'll be too dead to tell you.");
    // pull
    execPromise('sudo -u deploy /srv/deploy/deploy_bad_janet.sh')
      // then post results
      .then(result => {
        msg.send(result);
        msg.send('Am I alive? Yes! I did it! I have deployed.');
        return true;
      })
      .catch(err => {
        msg.send(err);
      });
  });
};

function execPromise(cmd) {
  const exec = require('child_process').exec;
  return new Promise((resolve, reject) => {
    exec(cmd, {cwd: dir}, function (error, stdout, stderr) {
      if (error) {
        console.warn(stderr);
        return reject(stderr);
      } else {
        return resolve(stdout);
      }
    });
  });
}
