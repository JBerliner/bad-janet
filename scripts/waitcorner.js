// Description:
//    Updates your corner wait time to 2 hours from time of command
//
// Commands:
//    ?waitcorner

const shame = require('../src/shame.js');

/**
 * ?waitcorner
 */
module.exports = function(robot) {
  robot.hear(/^\?waitcorner/, function(msg) {
    let raw_message = robot.discordJsMessage;
    let sender = raw_message.author; // User object
    shame.userHasShameByID(raw_message.author)
      .then(flag => {
        if (!flag) {
          raw_message.reply('What? You don\'t have that role.');
        } else {
          shame.shameUpdate(sender.id, 7200000);
          raw_message.delete();
          msg.send(`<@${sender.id}> has chosen to wait for their corner of shame to end.`);
        }
      })
  });
}
