// Description:
//    send flowers
//
// Commands:
//    block?
//    block user_id
//    unblock user_id
//    block all
//    unblock all
//    block list

const helpers = require('../src/helpers.js');
const admin_room = process.env.ADMIN_ROOM;
const MessageStacker = require('../src/classes/MessageStacker.js');

module.exports = function(robot) {
  /**
   * block?
   */
  robot.respond(/block\?/i, function() {
    robot.discordJsMessage.reply('If someone is abusing the flowers command you can '
      + 'block them using block commands. Blocking specific users is **silent**, and '
      + 'does not tell the target they\'ve been blocked. Blocking flowers from all users '
      + 'will let anyone who tries to send you some know you aren\'t accepting them.'
      + '\n'
      + '\n`block (userID)` Block a specific user.'
      + '\n`block all` Stop accepting flowers entirely.'
      + '\n`unblock (userID)` Unblock a specific user.'
      + '\n`unblock all` Start accepting flowers again.'
      + '\n`block list` See everyone you\'ve blocked.'
    );
  });

  /**
   * block audit
   */
  robot.hear(/^block audit$/i, async function (msg) {
    if (admin_room && admin_room !== msg.message.room) {
      robot.discordJsMessage.reply("You can't do that here.");
    } else {
      const list = helpers.brainKeyExists('block_list');
      const stacker = new MessageStacker('');

      for (let blocker_id in list) {
        for (let blocked_id in list[blocker_id]) {
          if (blocked_id === 'all') {
            stacker.appendOrSend(`<@${blocker_id}> has blocked **everyone**.\n`);
          } else {
            stacker.appendOrSend(`<@${blocker_id}> has blocked <@${blocked_id}>.\n`);
          }
        }
      }
      stacker.finalize();
    }
  });

  /**
   * block list
   */
  robot.respond(/block list/i, function(msg) {
    let block_list = helpers.brainKeyExists('block_list', msg.message.user.id);
    let base_text = '';
    let user_list = '';
    let noone = true;
    let split = false;

    // Add a message to the top if they blocked all flowers
    if (block_list.all) {
      base_text = '**You\'ve blocked all incoming flowers.**\nTo undo this, use `unblock all`. '
        + '\n'
        + '\n';
    }

    // List specific blocked users
    for (let key in block_list) {
      if (key !== 'all') {
        // Add the header for the list if it's not empty
        if (noone) {
          base_text = base_text + 'You\'ve blocked these users specifically:'
          noone = false;
        }

        // Split the message if it's over 2000 characters
        let test_message = base_text + user_list + `\n<@${block_list[key].id}>, to unblock them use \`unblock ${key}\``;
        if (test_message.length > 2000) {
          robot.discordJsMessage.reply(base_text + user_list);
          base_text = '';
          user_list = '';
          split = true;
        }

        user_list += `\n<@${block_list[key].id}>, to unblock them use \`unblock ${key}\``
      }
    }

    if (noone) {
      base_text = base_text + 'You haven\'t blocked any specific users yet. DM me `block?` for details about block users.';
      robot.discordJsMessage.reply(base_text);
    } else if (!split) {
      robot.discordJsMessage.reply(base_text + user_list);
    }
  });

  /**
   * block (user)
   */
  robot.respond(/block \d+/i, function(msg) {
    robot.brain.setAutoSave(true);
    let block_list = helpers.brainKeyExists('block_list', msg.message.user.id);
    let text = msg.message.text;
    let blocked_user_id = text.match(/\d+/i);
    blocked_user_id = blocked_user_id[0];

    if (block_list[blocked_user_id]) {
      robot.discordJsMessage.reply(`<@${blocked_user_id}> is already blocked from sending you flowers.`);
    } else {
      block_list[blocked_user_id] = {
        'id': blocked_user_id,
        'time': new Date()
      }
      robot.discordJsMessage.reply(`<@${blocked_user_id}> is now blocked from sending you flowers.`);
    }
  });

  /**
   * block all
   */
  robot.respond(/block all/i, function(msg) {
    robot.brain.setAutoSave(true);
    let block_list = helpers.brainKeyExists('block_list', msg.message.user.id);

    if (block_list.all) {
      msg.robot.discordJsMessage.reply('You\'ve already blocked everyone.');
    } else {
      block_list.all = {
        'time': new Date()
      }
      msg.robot.discordJsMessage.reply('Blocked everyone from sending you flowers.');
    }
  });

  /**
   * unblock (user)
   */
  robot.respond(/unblock \d+/i, function(msg) {
    robot.brain.setAutoSave(true);
    let block_list = helpers.brainKeyExists('block_list', msg.message.user.id);
    let text = msg.message.text;
    let blocked_user_id = text.match(/\d+/i);
    blocked_user_id = blocked_user_id[0];

    if (block_list[blocked_user_id]) {
      delete block_list[blocked_user_id];
      robot.discordJsMessage.reply(`<@${blocked_user_id}> can send you flowers again.`);
    } else {
      robot.discordJsMessage.reply(`<@${blocked_user_id}> is already unblocked.`);
    }
  });

  /**
   * unblock all
   */
  robot.respond(/unblock all/i, function(msg) {
    robot.brain.setAutoSave(true);
    let block_list = helpers.brainKeyExists('block_list', msg.message.user.id);

    if (block_list.all) {
      delete block_list.all;
      robot.discordJsMessage.reply('You are now accepting flowers from users you haven\'t specifically blocked.');
    } else {
      robot.discordJsMessage.reply('You are already accepting flowers from users you haven\'t specifically blocked.');
    }
  });
}
