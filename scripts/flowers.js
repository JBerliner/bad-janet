// Description:
//    send flowers
//
// Commands:
//    flowers @user message

const { MessageEmbed } = require('discord.js');
const moderation_room = process.env.MODERATION_ROOM;

const flowers = [
  'https://cdn.discordapp.com/attachments/928079339482923010/928256427204698162/1e0e705d-5d7e-4d66-b097-154e0d57de20_1.png',
  'https://cdn.discordapp.com/attachments/928079339482923010/928256427640897536/flower-shop-always-on-my-mind-65-167398.png',
  'https://cdn.discordapp.com/attachments/928079339482923010/928256427921899520/Deluxe_PurpleFlowerBouquet.png',
  'https://cdn.discordapp.com/attachments/928079339482923010/928256428119035934/sku4601408.png',
  'https://cdn.discordapp.com/attachments/928079339482923010/928256428756586506/Deluxe_PinkFlowerBouquet.png',
  'https://cdn.discordapp.com/attachments/928079339482923010/928256428962091058/flower-bouquet-delivery-in-athens.png',
  'https://cdn.discordapp.com/attachments/928079339482923010/928256429327024138/Diamond-Embroidery-Beautiful-Flower-Bouquet-Picture-of-Rhinestone-Full-Square-Diamond-Mosaic-Floral-Series-Bedroom-Decor.png',
]

/**
 * flowers @user (optional message)
 */
module.exports = function(robot) {
  robot.hear(/^\?flowers/i, function(msg) {
    let message = msg.message;
    let raw_message = robot.discordJsMessage;
    let content = raw_message.content;

    // Get front half of the message
    let front_half = content.match(/^\?flowers <@\d+>\s?/i);

    if (!front_half) {
      msg.reply('Flowers command not formed correctly. The command is `?flowers @usertag Optional message`');
    } else {
      let mentions = raw_message.mentions;
      let to_user = mentions.users.first(); // User object
      let card = content.replace(front_half, '');

      let sender = raw_message.author; // User object
      let card_message = `**<@${sender.id}> has sent you flowers!**`;
      if (card) {
        card = card.replace(/\n/g, '\n> ');
        card_message = card_message + `\nThe card says: \n> ${card}`;
      }

      let chosen_flowers = msg.random(flowers);

      let block_list = robot.brain.data.block_list;
      let color = '';
      let status = '';
      if (block_list
        && block_list[to_user.id]
        && block_list[to_user.id][sender.id]
        && block_list[to_user.id][sender.id].id
        && block_list[to_user.id][sender.id].id == sender.id
      ) {
        status = 'BLOCKED';
        color = '#ff050d';
      } else if (block_list
        && block_list[to_user.id]
        && block_list[to_user.id].all
      ) {
        msg.send('I\'m sorry, the user you are trying to send flowers to is not accepting any flowers at this time.');
        status = 'Blocked All';
        color = '#000000';
      } else {
        const embededMessage = new MessageEmbed()
          .setColor('#bfc0ff')
          .setTitle('You\'ve got flowers!')
          .setImage(chosen_flowers)
          .setDescription(card_message)
          .setTimestamp()
          .setFooter('If you would like information on blocking users, send me a message `block?`, \nor `block '
          + sender.id
          + '` to block this user.')
        ;
        to_user.send({ embeds: [embededMessage] })


        status = 'Sent';
        color = '#06d622';
      }

      let cardline = '';
      if (card) {
        cardline = `Card: \n> ${card}`;
      } else {
        cardline = 'No card message attached';
      }

      // Copy moderation channel
      const channel = robot.client.channels.cache.get(moderation_room);
      const embededMessage = new MessageEmbed()
        .setColor(color)
        .setTitle(`Flowers Sent by ${message.user.nickname}`)
        .setDescription(`
        User: <@${message.user.id}>
        Original message: ${message.text}
        To: <@${to_user.id}>
        ${cardline}`)
        .setTimestamp()
        .setFooter(`Status: ${status}`)
      ;
      channel.send({ embeds: [embededMessage] })


      raw_message.delete();
    }
  });
}
