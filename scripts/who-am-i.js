// Description:
//    Who are you?
//
// Dependencies:
//    None
//
// Configuration:
//    None
//
// Commands:
//    hubot who am I?

module.exports = function(robot) {
  /**
   * hubot who am i?
   */
  robot.respond(/who am i\??/gi, function(msg) {
    let user_id = msg.message.user.id
    let brain_users = robot.brain.data.users;
    let user = brain_users[user_id];
    msg.send(`I remember you <@${user.id}>.
id: ${user.id}
name: ${user.name}
handle: ${user.nickname}
discriminator: ${user.discriminator}`);

    robot.client.users.fetch(user.id)
      .then(user_js => {
        msg.send(`Discord.js found <@${user.id}>:
id: ${user_js.id}
username: ${user_js.username}
discriminator: ${user_js.discriminator}
avatar: ${user_js.avatar}
        `);

        let name = '';
        if (user_js.displayName) {
          name = user_js.displayName;
        } else {
          name = user_js.username;
        }
        brain_users[user_js.id] = {
          'id': user_js.id,
          'name': name,
          'nickname': user_js.displayName,
          'original_username': user_js.username,
          'discriminator': user_js.discriminator,
          'bot': user_js.bot
        };
      });
  });
}
