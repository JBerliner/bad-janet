// Description:
//    Rename a consentee via commands
//
// Commands:
//    rename @user

const consent = require('../src/consent.js');
const reload = require('../src/reload_users_in_brain.js');
const helpers = require('../src/helpers.js');

/**
 * rename @user (new name)
 */
module.exports = function(robot) {
  robot.respond(/rename @/i, function(msg) {
    let raw_message = robot.discordJsMessage;
    let content = raw_message.content;
    // Get the front half of the message.
    let front_half = content.match(/rename <@\d+>\s?/i);
    if (front_half) {
      let mentions = raw_message.mentions;
      let mentioned_user = mentions.users.first(2)[1]; // User object
      let sender = raw_message.author; // User object
      if (consent.hasConsent(sender, mentioned_user)) {
        let nickname = content.replace(front_half, '');
        nickname = nickname.replace(/<@\d+>\s?/i, '');
        let brain_data = robot.brain.data;
        robot.client.guilds.cache.get(process.env.GUILD_ID).members.fetch(mentioned_user.id)
          .then(user => {
            if (nickname.length > 31) {
            // Can only be 32 characters long
              nickname = nickname.substring(0, 31);
              raw_message.react('⚠️');
              raw_message.reply('New name has been truncated to \''
            + nickname
            + '\'. Only 32 characters allowed.');
            }

            // Save the brain data before renaming
            helpers.brainKeyExists('renamed_subs');
            brain_data.renamed_subs[mentioned_user.id] = {
              'old_name': user.displayName,
              'new_name': nickname,
              'changed_by': sender.id,
              'time': new Date()
            };
            robot.brain.save();
            return user;
          }).then(user => {
            user.setNickname(nickname)
              .then(guildMemberManager => {
                raw_message.react('👍');
                reload.reloadAllUsersInBrain(robot);
              })
              .catch(error => {
                console.log(error);
                raw_message.react('❌');
                // reset the brain
                delete brain_data.renamed_subs[mentioned_user.id];
                robot.brain.save();
              });
          }).catch(error => {
          // Do I need this catch block? I don't know! Promises are confusing
            console.log(error);
            raw_message.react('❌');
            // Reset the brain
            delete brain_data.renamed_subs[mentioned_user.id];
            robot.brain.save();
          })
        ;
      } else {
        raw_message.react('🚫');
        raw_message.reply('You do not have consent to rename that user.');
      }
    }
  });
}
