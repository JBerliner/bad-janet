// Description:
//    Create booths for users, move booths to top for new messages
//
// Commands:
//    booth @user
//    register booth

const helpers = require('../src/helpers.js');

const admin_room = process.env.ADMIN_ROOM;
const category = process.env.BOOTH_CATEGORY;

module.exports = function(robot) {
  /**
   * booth (userID)
   */
  robot.respond(/booth @/i, function(msg) {
    if (admin_room && admin_room !== msg.message.room) {
      msg.send("You can't do that here.");
      return;
    }

    let raw_message = robot.discordJsMessage;

    let content = raw_message.content;
    content.match(/booth <@\d+>/i);
    let mentions = raw_message.mentions;
    let mentioned_user = mentions.users.first(2)[1]; // User object

    const server = raw_message.guild;
    const name = mentioned_user.username;

    server.channels.create('under-construction', 'text')
      .then(channel => {
        // Move channel to category
        channel.setParent(category);
        channel.permissionsFor();

        // Remove perms from @everyone to view the channel, message in it, or create threads
        channel.permissionOverwrites.create(channel.guild.roles.everyone, {
          VIEW_CHANNEL: false,
          SEND_MESSAGES: false,
          CREATE_PUBLIC_THREADS: false,
          CREATE_PRIVATE_THREADS: false,
        });

        // Give the mentioned user the ability to manage the channel and create threads
        channel.permissionOverwrites.create(mentioned_user, {
          MANAGE_CHANNELS: true,
          SEND_MESSAGES: true,
          CREATE_PUBLIC_THREADS: true,
          CREATE_PRIVATE_THREADS: true,
          MANAGE_MESSAGES: true
        });

        // Allow Typists, Task Setters, and Switches to view the channel
        let role = server.roles.cache.find(role => role.name === 'Typist');
        channel.permissionOverwrites.create(role, {
          VIEW_CHANNEL: true,
        });
        role = server.roles.cache.find(role => role.name === 'Task Setter');
        channel.permissionOverwrites.create(role, {
          VIEW_CHANNEL: true,
        });
        role = server.roles.cache.find(role => role.name === 'Switch');
        channel.permissionOverwrites.create(role, {
          VIEW_CHANNEL: true,
        });

        // Don't let quarantine members do anything, blocks the owner from using their booth while quarantined
        role = server.roles.cache.find(role => role.name === 'quarantine');
        channel.permissionOverwrites.create(role, {
          SEND_MESSAGES: false,
          VIEW_CHANNEL: false,
          READ_MESSAGE_HISTORY: false,
        });

        // Set the booth name
        channel.setName(name);

        // Register the booth to the brain
        robot.brain.setAutoSave(true)
        let brain_data = robot.brain.data;
        helpers.brainKeyExists('booths');
        brain_data.booths[channel.id] = {
          'id': channel.id,
          'last_updated': new Date()
        };
      });
  });

  /**
   * register booth
   */
  robot.respond(/register booth/i, function(msg) {
    let room = msg.message.room;
    robot.brain.setAutoSave(true)
    let brain_data = robot.brain.data;
    helpers.brainKeyExists('booths');
    brain_data.booths[room] = {
      'id': room,
      'last_updated': new Date()
    };

    // Delete the original message
    robot.discordJsMessage.delete();
  });

  /**
   * Move booths up for new messages
   */
  robot.hear(/.*/, function(msg) {
    // Only continue if the channel is in a registered booth
    if (robot.brain.data.booths && robot.brain.data.booths[msg.message.room]) {
      // Ignore the message if it was sent by a bot
      let user = msg.message.user;
      if (!user.bot) {
        robot.client.guilds.cache.get(process.env.GUILD_ID).channels.fetch(msg.message.room)
          .then(channel => {
            channel.setPosition(0);
          });
      }
    }
  });
}
