// Description:
//    Looks at all the users in the guild and loads them into memory.
//
// Dependencies:
//    None
//
// Configuration:
//    None
//
// Commands:
//    hubot memorize all users

const reload = require('../src/reload_users_in_brain.js');

module.exports = function(robot) {
  robot.respond(/memorize all users/i, function(msg) {
    reload.reloadAllUsersInBrain(robot, msg);
  });
}
