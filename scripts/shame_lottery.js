// Description:
//   manage the lottery for the corner of shame
//
// Commands:
//   ?joinlottery - you can join the lottery
//   ?leavelottery - you can leave the lottery
//   ?listlottery - lists everyone in the lottery
//   ?drawlottery - draw one name from the lottery, finished the lottery and drops all the names
//   ?draw2lottery - use any number of people to draw

const shame_role = process.env.SHAME_ROLE;
let helpers = require('../src/helpers.js');

/* global robot */

const suspense = [
  'Are you on the edge of your seat?',
  'Just swishing the names around here.',
  'Gotta add some suspense here, ya know.',
  'You ready?',
  'Hold on a moment',
  'I love to make you wait.',
  'Patience.',
  'Shake shake shaking up the names... ',
  'Drumroll please... ',
  'Don\'t want to wait anymore? Suffer.',
  'Just another moment.',
  'Wait for it...',
  'https://tenor.com/view/lets-see-what-happens-steve-kornacki-msnbc-lets-wait-for-it-were-gonna-wait-for-the-result-gif-19694593',
  'https://tenor.com/view/lottery-lotto-gif-12706150',
  'https://tenor.com/view/suspense-waiting-wait-mystery-daily-show-gif-22337508',
  'https://tenor.com/view/willy-wonka-excited-smile-the-suspense-gene-wilder-gif-5933521',
  'https://tenor.com/view/psych-shawn-shawn-spencer-james-roday-wait-gif-8535911',
  'Huh? Oh! Spaced out there a second.',
  'SHAME SHAME SHAME on you for being so impatient.',
  'Oh hold on, I shook up my grocery list instead of the list of names.',
  'https://tenor.com/view/wait-for-it-barney-stinson-himym-neil-patrick-harris-gif-6055274',
  'https://tenor.com/view/tea-shade-goth-halloween-you-just-wait-gif-15826642',
  'https://tenor.com/view/lafuddyduddy-drum-duck-cute-drum-roll-please-gif-16950134',
  'This definitely needs a few more seconds of shaking around.',
  "Oops, I dropped a few, give me a moment to make sure they're still all here.",
];

const chooseString = [
  `Someone needs to go to the ${shame_role} and it's going to be `,
  'I pulled a name and the loser is ',
  'SHAME SHAME SHAME on ',
  'And the loser is ',
];

const pause_timer = 7000;
let count = 1;
let keys = [];

/**
 * ?joinlottery
 */
module.exports = function(robot) {
  robot.hear(/^\?join[lI]ottery/i, function(msg) {
    robot.brain.setAutoSave(true)
    let brain_data = robot.brain.data;
    let user_id = msg.message.user.id
    helpers.brainKeyExists('corner_line');
    if (brain_data.corner_line[user_id]) {
      msg.send("You're already in the lottery of shame and you can't join it twice.");
    } else {
      brain_data.corner_line[user_id] = user_id;
      msg.send("You've been added to the lottery.");
    }
  });

  /**
   * ?leavelottery
   */
  robot.hear(/^\?leavelottery/i, function(msg) {
    robot.brain.setAutoSave(true)
    let brain_data = robot.brain.data;
    let user_id = msg.message.user.id
    helpers.brainKeyExists('corner_line');
    if (brain_data.corner_line[user_id]) {
      msg.send("Coward. You've been removed from the lottery but you should go write some lines for chickening out.");
      delete brain_data.corner_line[user_id];
    } else {
      msg.send("You aren't even entered.");
    }
  });

  /**
   * ?listlottery
   */
  robot.hear(/^\?listlottery/i, function(msg) {
    let brain_data = robot.brain.data;
    let list = '';
    for (let key in brain_data.corner_line) {
      list = list + `<@${key}>, `;
    }
    if (list == '') {
      msg.send('No one is entered just yet. Maybe you should add yourself?');
    } else {
      msg.send('Current list of users in the lottery: ' + list);
    }
  });

  /**
   * ?drawlottery / ?draw2lottery
   */
  robot.hear(/^\?draw\d*lottery/i, function(msg) {
    let input_message = msg.match[0];
    let matched = input_message.match(/\d+/i);
    if (matched) {
      count = matched || 1;
    }

    robot.brain.setAutoSave(true)
    let brain_data = robot.brain.data;
    let list = '';
    for (let key in brain_data.corner_line) {
      list = list + `<@${key}>, `;
    }
    if (list == '') {
      msg.send('No one is entered just yet. Maybe you should add yourself?');
    } else {
      // https://stackoverflow.com/a/49687370
      keys = Object.keys(brain_data.corner_line);
      let length = keys.length;
      if (length < count) {
        count = length;
      }
      if (length == 1) {
        msg.send("Really? There's only one name. Okay. I guess I'll shake up the list that consists of one entire name and choose one.");
        addSuspense(msg);
      } else if (length == count) {
        msg.send(`Really? The list only has ${count} names. Not very fair for everyone is it? Okay. I guess I'll shake up the list and choose *everybody*.`);
        addSuspense(msg);
      } else {
        msg.send('Okay give me a moment to shake up the ' + length + ` names and I'll choose ${count}.`);
        addSuspense(msg);
      }
    }
  });
}

function addSuspense(msg) {
  let brain_data = robot.brain.data;
  setTimeout(function() {
    msg.send(msg.random(suspense));
    // 50/50 chance whether there will be a second suspense
    // Unless the count is 1 at which point, suffer
    let x = (Math.floor(Math.random() * 2) == 0);
    if (x || count == 1) {
      setTimeout(function() {
        msg.send(msg.random(suspense));
        setTimeout(function() {
          chooseNames(msg)
            .then(variable => {
              brain_data.corner_line = {};
            });
        }, pause_timer);
      }, pause_timer);
    } else {
      setTimeout(function() {
        chooseNames(msg)
          .then(variable => {
            brain_data.corner_line = {};
          });
      }, pause_timer);
    }
  }, pause_timer);
}


async function chooseNames(msg) {
  let brain_data = robot.brain.data;
  for (let i = 0; i < count; i++) {
    keys = Object.keys(brain_data.corner_line);
    let randIndex = Math.floor(Math.random() * keys.length);
    let randKey = keys[randIndex];

    await robot.client.guilds.cache.get(process.env.GUILD_ID).members.fetch(randKey)
      .then(member => {
        let role = member.guild.roles.cache.find(role => role.name === shame_role);
        member.roles.add(role);
        delete brain_data.corner_line[randKey];
        msg.send(msg.random(chooseString) + ` <@${randKey}>!`);
      });
  }
}
