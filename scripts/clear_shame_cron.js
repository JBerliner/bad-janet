// Description:
//   Clears out any set shame roles in case the timer doesn't do it.
//
// Dependencies:
//   node-cron
//
// Configuration:
//   None
//
// Commands:
//   None


const cron = require('node-cron');
const shame_role = process.env.SHAME_ROLE;
const admin_room = process.env.ADMIN_ROOM;
const helpers = require('../src/helpers.js');

module.exports = function(robot) {
  // Every 15 minutes
  cron.schedule('1 0,15,30,45 * * * *', () => {
    robot.brain.setAutoSave(true);
    let shame_list = helpers.brainKeyExists('count_fail_shame_list');
    let keys = Object.keys(shame_list);

    for (let key of keys) {
      let row = shame_list[key];
      let over_time = new Date(row.over);

      if (over_time <= new Date()) {
        // Corner time is over, remove the shame role
        robot.client.guilds.cache.get(process.env.GUILD_ID).members.fetch(key)
          .then(user => {
          // Remove the shame role from the user
            let role = user.guild.roles.cache.find(role => role.name === shame_role);
            if (user.roles.cache.some(role => role.name === shame_role)) {
            // only try to remove the role if the user has it
              user.roles.remove(role);
              console.log('Removed shame from ' + user.displayName);
            }
            // No matter what delete the brain key
            console.log('Removed shame key ' + user.displayName);
            delete robot.brain.data.count_fail_shame_list[key];
            robot.brain.save();
          })
          .catch(async err => {
            let user = await robot.client.guilds.cache.get(process.env.GUILD_ID).members.fetch(key);
            console.log(`Error removing shame role from <@${key}>\n` + err);
            robot.messageRoom(admin_room, `Error removing shame role from ${user.displayName}\n` + err);
          });
      }
    }
  });
}
