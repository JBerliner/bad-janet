// Description:
//    Reduces the slowdown for messing up the count by 1 every week
//
// Dependencies:
//    node-cron
//
// Configuration:
//    None
//
// Commands:
//    slow modify?
//    slow modifier?
//    hubot slow opt in
//    hubot slow opt out
//    hubot my slowdown

const cron = require('node-cron');
const helpers = require('../src/helpers.js');
const consent = require('../src/consent');
const admin_room = process.env.ADMIN_ROOM;
const Slowdown = require('../src/classes/Slowdown.js');

module.exports = function(robot) {
  /**
   * Cron to decrement slowdown timers every Monday night at midnight (I think)
   */
  cron.schedule('0 0 * * 1', () => {
    const list = helpers.brainKeyExists('count_fail_tracking');
    let template = 'Weekly cron to reduce fail counts has completed:';
    let message = template;

    for (let user_id in list) {
      if (robot.brain.data.count_fail_tracking[user_id] > 0) {
        robot.brain.data.count_fail_tracking[user_id]--;
        message += `<@${user_id}> slowdown: ${robot.brain.data.count_fail_tracking[user_id] - 1}s\n`;
      }
    }

    if (message !== template) {
      robot.brain.data.save();
      const channel = robot.client.channels.cache.get(admin_room);
      channel.send(message);
    }
  });

  /**
   * slow modify? / slow modifier?
   */
  robot.hear(/^(@Bad Janet\s?)?slow\s?modif(ier|y)\??/i, function(msg) {
    robot.discordJsMessage.reply('Every time you miscount you\'ll type one second '
      + 'slower in the corner of shame. This slowdown isn\'t permanent — you\'ll lose '
      + 'one second every week. **These seconds add on to any slowdown set by another '
      + 'user!** Everyone is opted into this system by default, but you can opt out '
      + 'if you prefer. You can also also DM Bad Janet if you don\'t want people '
      + 'people to know you\'re a huge chicken. 🐔'
      + '\n'
      + '\n `@Bad Janet my slow down` to see your current modifier.'
      + '\n `@Bad Janet slow opt out` to opt out of this system.'
      + '\n `@Bad Janet slow opt in` to opt back into this system.'
    );
  });

  /**
   * hubot slow opt out
   */
  robot.respond(/slow\s?opt\s?out/i, function() {
    let author = robot.discordJsMessage.author;

    if (!robot.brain.data.count_fail_opt_out[author.id]) {
      robot.brain.data.count_fail_opt_out[author.id] = {
        'id': author,
        'opt_out': new Date()
      }

      robot.brain.save();
      robot.discordJsMessage.react('✅');
      robot.discordJsMessage.reply('You\'re now opted out of the slowdown. 🐔');
    } else {
      robot.discordJsMessage.react('🚫')
      robot.discordJsMessage.reply('You\'re already opted out of the slowdown. 🐔');
    }
  });

  /**
   *hubot slow opt in
  */
  robot.respond(/slow\s?opt\s?in/i, function() {
    let author = robot.discordJsMessage.author;

    if (robot.brain.data.count_fail_opt_out[author.id]) {
      delete robot.brain.data.count_fail_opt_out[author.id];

      robot.brain.save();
      robot.discordJsMessage.react('✅');
      robot.discordJsMessage.reply('You\'re now opted back into the slowdown.');
    } else {
      robot.discordJsMessage.react('🚫')
      robot.discordJsMessage.reply('You\'re already opted into the slowdown.');
    }
  });

  /**
   * hubot my slowdown
   */
  robot.respond(/my\s?slow\s?down/i, async function (msg) {
    let author = robot.discordJsMessage.author;
    let modifierList = helpers.brainKeyExists('slowdown', author.id);

    let countSlowdown = 0;
    let userSlowdown = 0;
    let setter = '';

    helpers.brainKeyExists('count_fail_opt_out');
    helpers.brainKeyExists('count_fail_tracking');

    // Set the reply for the miscounting part of the string
    let countReply = '';
    if (robot.brain.data.count_fail_tracking[author.id]) {
      const slowdownClass = new Slowdown(author);
      countSlowdown = slowdownClass.getCountModifierSeconds();
      if (countSlowdown === 0) {
        countReply = 'Your first miscount is on the house.'
      } else {
        countReply = `${countSlowdown} seconds are from miscounting.`;
      }
    } else {
      countReply = 'You haven\'t miscounted yet.';
    }

    // Find the highest slowdown set by another user
    for (let top_id in modifierList) {
      // If the slowdown from this setter is higher than the one before
      if (modifierList[top_id].seconds > userSlowdown) {
        // If they set their own slowdown or have consented to the new setter
        if (consent.hasConsentByID(top_id, author.id) || modifierList[top_id].self) {
          userSlowdown = modifierList[top_id].seconds;
          let user = await robot.client.guilds.cache.get(process.env.GUILD_ID).members.fetch(top_id);
          setter = `${user.displayName}`;
        }
      }
    }

    // Set the reply for slowdowns set by other users
    let userReply = '';
    if (userSlowdown > 0) {
      userReply = `You can thank **${setter}** for the other ${userSlowdown} 💖`;
    } else {
      userReply = 'Noone\'s slowed you down yet';
    }

    // If the user has slowdown from miscounting or another user
    let reply = '';
    if (countSlowdown > 0 || userSlowdown > 0) {
      // Create the full reply
      reply = `Your total slowdown is **${+countSlowdown + +userSlowdown}** seconds.`
        + `\n${countReply}`
        + `\n${userReply}`;
    } else {
      reply = 'You don\'t have any slowdown. Good for you!';
    }

    // Send the completed reply based on if the user opted out or not
    if (robot.brain.data.count_fail_opt_out[author.id]) {
      robot.discordJsMessage.reply('You have opted out of the slowdown.\nYou can opt back in with `slow opt in`.');
    } else {
      robot.discordJsMessage.reply(reply);
    }
  });
}
