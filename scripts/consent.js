// Description:
//    Consent for user to use other commands on sending user
//
// Commands:
//     consent to @user
//     end consent to @user
//     consent to everyone
//     end consent to everyone
//     safeword|red|pineapple
//     end safeword

const consent = require('../src/consent.js');
const helpers = require('../src/helpers.js');
const GUILD = process.env.GUILD_ID;

/**
 * consent?
 */
module.exports = function(robot) {
  robot.respond(/consent\?$/i, function(msg) {
    let raw_message = robot.discordJsMessage;
    raw_message.reply(`**Consent to Mess With**

:small_orange_diamond:\`@Bad Janet consent @user\` or \`@Bad Janet consent to @user\` - Consent to let the mentioned user mess with you using Bad Janet commands

:small_orange_diamond:\`@Bad Janet end consent @user\` or \`@Bad Janet end consent to @user\` - End consent to let the mentioned user mess with you using Bad Janet commands

:small_orange_diamond:\`@Bad Janet consent all\` or \`@Bad Janet consent to all\` - consent to let all users mess with you using Bad Janet commands

:small_orange_diamond:\`@Bad Janet end consent all\` or \`@Bad Janet end consent to all\` - End consent to let all users mess with you using Bad Janet commands

:small_orange_diamond:\`@Bad Janet safeword\` or \`@Bad Janet red\` or \`@Bad Janet pineapple\` - Pause all consent. No one can mess with you right now.

:small_orange_diamond:\`@Bad Janet end safeword\` - Unpause all consent. Anyone you've authorized can mess with you again.

**Messing with**
:small_blue_diamond:\`@Bad Janet shame @user\` - shame that user as if they'd asked for it themselves (24h or until they do their counting in the corner)

:small_blue_diamond:\`@Bad Janet rename @user new name\` - rename that user to something more suitable. Max 32 characters.

:small_blue_diamond:More to come . . . :smiling_imp:

** Other Commands**
:diamond_shape_with_a_dot_inside:\`@Bad Janet given consent\`- See all you've given consent to (does *not* tag/ping users)

:diamond_shape_with_a_dot_inside:\`@Bad Janet received consent\`- See all you've received consent from (does *not* tag/ping users)

:diamond_shape_with_a_dot_inside:Coming soon. . . list top users with most consent`);
  });

  robot.respond(/end consent$/i, function(msg) {
    let raw_message = robot.discordJsMessage;
    raw_message.reply('Command not correctly formed. Did you forget to tag who you want to end consent to?');
  });

  robot.respond(/consent$/i, function(msg) {
    let raw_message = robot.discordJsMessage;
    raw_message.reply('Command not correctly formed. Did you forget to tag who you want to consent to?');
  });

  /**
   * consent to @user / all
   */
  robot.respond(/consent (to )?/i, function(msg) {
    let sender = msg.message.user;
    let raw_message = robot.discordJsMessage;
    let content = raw_message.content;
    let consent_list = helpers.brainKeyExists('consent', sender.id);

    // Get the front half of the message
    let front_half = content.match(/consent (to )?<@\d+>/i);
    if (!front_half) {
      let everyone = content.match(/consent (to )?all/i);
      if (everyone) {
        consent_list.everyone = {
          'time': new Date(),
        }
        msg.send(`Everyone has been given consent to mess with <@${sender.id}> using my commands.`);
      } else {
        msg.send('Command not formed correctly. The command is '
          + '\n> <@929783745513406504> consent to @usertag\n or'
          + '\n> <@929783745513406504> consent to all');
      }
    } else {
      let mentioned_user_id = helpers.getFirstUserIdFromMentionOnRespond(raw_message);
      consent_list[mentioned_user_id] = {
        'id': mentioned_user_id,
        'time': new Date(),
      }
      msg.send(`<@${mentioned_user_id}> has been given consent to mess with <@${sender.id}> `
        + `using my commands. To revoke consent use \n> <@929783745513406504> end consent to <@${mentioned_user_id}>`);
    }
  });

  /**
   * end consent to @user / all
   */
  robot.respond(/end consent (to )?/i, function(msg) {
    let sender = msg.message.user;
    let raw_message = robot.discordJsMessage;
    let content = raw_message.content;
    let consent_list = helpers.brainKeyExists('consent', sender.id);

    // Get the front half of the message
    let front_half = content.match(/end consent (to )?<@\d+>/i);
    if (!front_half) {
      let everyone = content.match(/end consent (to )?all/i);
      if (everyone) {
        if (consent_list.everyone) {
          delete consent_list.everyone;
          msg.send('Consent to everyone has been revoked.');
          return;
        } else {
          msg.send('I couldn\'t find consent to everyone so I haven\'t changed anything. If this is a mistake, bother beta about it.');
          return;
        }
      }
      msg.send('Command not formed correctly. The command is \n> <@929783745513406504> end consent to @usertag');
    } else {
      let mentioned_user_id = helpers.getFirstUserIdFromMentionOnRespond(raw_message);
      if (consent_list[mentioned_user_id]) {
        delete consent_list[mentioned_user_id];
        msg.send(`Consent to <@${mentioned_user_id}> has been revoked.`);
      } else {
        msg.send('I wasn\'t able to find any consent on record to that user. If this is a mistake, bother beta about it.');
      }
    }
  });

  /**
   * safeword / red / pineapple
   */
  robot.respond(/(safeword|red|pineapple)/i, function(msg) {
    let sender = msg.message.user;
    let consent_list = helpers.brainKeyExists('consent', sender.id);

    consent_list.safeword = true;
    msg.send('Okay. No one can mess with you right now. Take a break. '
      + 'If/when you want to end your safeword, send \n> <@929783745513406504> end safeword');
  });

  /**
   * end safeword
   */
  robot.respond(/end safeword/i, function(msg) {
    let sender = msg.message.user;
    let consent_list = helpers.brainKeyExists('consent', sender.id);

    if (consent_list.safeword) {
      delete consent_list.safeword;
      msg.send('Okay. Your consent settings are still as you set them but people are allowed to mess with you again if you\'ve given them consent.');
    }
  });

  /**
   * received consent
   */
  robot.respond(/received consent/i, async function(msg) {
    let raw_message = robot.discordJsMessage;
    let sender = msg.message.user;
    consent.brainConsentUser(sender);
    let consent_list = robot.brain.data.consent;
    let this_user_tops_to = '';
    for (let sub_id in consent_list) {
      for (let top_id in consent_list[sub_id]) {
        if (top_id == sender.id || top_id == 'everyone') {
          try {
            let sub = await robot.client.guilds.cache.get(GUILD).members.fetch(sub_id);
            this_user_tops_to += `${sub.displayName}` + (top_id == 'everyone' ? ' (everyone)' : '') + ', ';
            console.log(this_user_tops_to);
          } catch { }
        }
      }
    }
    if (this_user_tops_to.length > 0) {
      raw_message.reply(`You've received consent from: ${this_user_tops_to}`);
    } else {
      raw_message.reply('You haven\'t received consent from anyone yet.');
    }
  });

  /**
   * given consent
   */
  robot.respond(/given consent/i, async function(msg) {
    let raw_message = robot.discordJsMessage;
    let sender = msg.message.user;
    consent.brainConsentUser(sender);
    let consent_list = robot.brain.data.consent;
    let this_user_subs_to = '';
    for (let sub_id in consent_list) {
      for (let top_id in consent_list[sub_id]) {
        if (sub_id == sender.id) {
          if (top_id == 'everyone') {
            this_user_subs_to += 'everyone, ';
          } else {
            try {
              let top = await robot.client.guilds.cache.get(GUILD).members.fetch(top_id);
              this_user_subs_to += `${top.displayName}, `;
            } catch { }
          }
        }
      }
    }
    if (this_user_subs_to.length > 0) {
      raw_message.reply(`You've given consent to: ${this_user_subs_to}`);
    } else {
      raw_message.reply('You haven\'t given consent to anyone yet.');
    }
  });
}
