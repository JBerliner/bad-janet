// Description:
//    Counts days until holiday
//
// Dependencies:
//    None
//
// Configuration:
//    None
//
// Commands:
//    hubot how many days until {holiday}?  - returns days left until day

module.exports = function(robot) {
  /**
   * how long until new year
   */
  robot.respond(/how (long|many days)\s?(left)?\s?(until|till|to)? (the)?\s?(new year|next year)'?s?\s?(day)?\s*\?*/i, function(msg) {
    processHoliday(msg, 'the New Year', '01/01/', 'Happy New Year!', 'Happy New Year!');
  });

  /**
   * how long until halloween
   */
  robot.respond(/how (long|many days)\s?(left)?\s?(until|till|to)? halloween\s*\?*/i, function(msg) {
    processHoliday(msg, 'Halloween', '10/31/', 'Happy Halloween!', 'Happy Halloween!');
  });

  /**
   * how long until christmas
   */
  robot.respond(/how (long|many days)\s?(left)?\s?(until|till|to)? (xmas|christmas)\s*\?*/i, function(msg) {
    processHoliday(msg, 'Christmas', '12/25/', 'Merry Christmas!', 'Happy Christmas Eve!');
  });
}

function processHoliday(msg, goal_name, goal_piece, greeting, greeting_eve) {
  let today = new Date();
  let goal_date = new Date(goal_piece + (today.getYear() + 1900));

  if (goal_date < today) {
    goal_date = new Date(goal_piece + (today.getYear() + 1901));
  }
  let time_until_goal_date = goal_date.getTime() - today.getTime();
  let days_until_goal_date = time_until_goal_date / (1000 * 3600 * 24);
  let full_days = Math.ceil(days_until_goal_date);
  if (full_days == 1) {
    msg.send('There is only one day left until ' + goal_name + '! ' + greeting_eve);
  } else if (full_days == 0) {
    msg.send("It's " + goal_name + ' today! ' + greeting);
  } else {
    msg.send('There are ' + full_days + ' days until ' + goal_name + '!');
  }
}
