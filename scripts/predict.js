// Description:
//    Predict the future
//
// Dependencies:
//    none
//
// Configuration:
//    none
//
// Commands:
//    hubot predict <query> - Obtain a response from the spirits


const predictions = [
  'Most likely',
  'Only if a task setter agrees.',
  'Of course!',
  'omg, YES!',
  'Either divine intervention or maybe the best programmers of discord told me to tell you: affirmative!',
  'Outlook is good.',
  'Yeah, why not?',
  'I guess so.',
  'Definitely.',
  'What? Oh. Sure, whatever.',

  // ----------------

  'I’m busy doing my own tasks. Ask me later.',
  'Hmmm. Maybe.',
  'You woke me up for this?',
  'What kind of a question is that?',
  'You really don\'t want to know the answer to that.',
  'I\'ll have to get back to you on that.',
  'You\'re kidding, right?',
  'Shouldn\'t you be on writeforme.org instead of asking silly questions?',
  'Better not tell you now',

  // ----------------

  'Don\'t count on it',
  'Ugh. That sounds like something a typist would ask. Definitely not.',
  'Nah, not really.',
  'My sources tell me no.',
  'No way.',
  'Doubtful.',
  'No. Do an oddjob for me instead.',
]

module.exports = function(robot) {
  robot.hear(/^predict\s/i, function(res) {
    res.send(res.random(predictions));
  });
};
