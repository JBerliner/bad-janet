// Description:
//   Assign a task every day
//
// Commands:
//   hubot add task
//   hubot remove task
//   hubot list tasks
//   hubot post task

/*
  Brain object outline:
  taskoftheday {
      id {
          url: url
          lines: line count
          submitted_by: @discord-user? discord user id?
          text: "task text"
          notes: comment or settings or anything else to display?
          last_posted_date: "YYYY-DD-MM" //requires logic to avoid posting same task too fast
      }
  }
 */

const { MessageEmbed } = require('discord.js');
const cron = require('node-cron');
const helpers = require('../src/helpers.js');
const MessageStacker = require('../src/classes/MessageStacker.js');

const run_time = process.env.DAILY_TASK_CRON || '1 0 0 * * *'; // 12:01am every day
const totd_room = process.env.DAILY_TASK_ROOM;
const admin_room = process.env.DAILY_TASK_ADMIN_ROOM;

module.exports = function(robot) {
  cron.schedule(run_time, () => {
    if (totd_room) {
      sendTask(robot);
    }
  });

  /**
   * Adds tasks to the brain.
   * You can add multiple tasks using "add task" as the splitter.
   * Feel free to add line breaks between tasks
   * for easy review but it's not necessary.
   *
   * Each task should be added using correct JSON
   */

  /**
   * add task (ID)
   */
  robot.respond(/add task [.\s\S]+/im, function(msg) {
    if (admin_room && admin_room != msg.message.room) {
      msg.send("You can't add tasks here.");
      return;
    }

    robot.brain.setAutoSave(true);
    let brain_data = robot.brain.data;
    let input_message = msg.match[0];

    // "add task" is the splitter
    let tasks = input_message.split('add task ');

    helpers.brainKeyExists('taskoftheday');

    function reject(error) {
      msg.send(error);
    }
    let id = false;
    for (let key in tasks) {
      if (key > 0) {
        // Skip first key because it's just the robot name

        // Why is this a promise??
        // Because I was trying to debug something that was the wrong problem
        // And I thought the problem was due to asynchronicity.
        // And I'm not putting it back.
        new Promise((resolve) => {
          // Trim any whitespace
          let setterJson = tasks[key].trim();
          try {
            // Send setters to next part of the promise chain
            resolve(JSON.parse(setterJson));
          } catch {
            reject("Uh. I don't think that's valid JSON. \nFormat should be:" +
            '`add task {VALID JSON}`\n\n' +
            'Key/pairs are `url`, `lines`, `submitted_by`, `text`, and `notes` ALL LOWER CASE.\n' +
            'All new lines in values should use `\\n`.');
          }
        }).then((setters) => {
          try {
            let url = setters.url;
            id = url.match(/([^/]+$)/gmi);
          } catch {
            id = false; // reset id
            reject("I can't find the URL; `url` is required.");
          }
          if (id) {
            // Only go on if an ID was found
            if (brain_data.taskoftheday[id]) {
              // If this task already exists
              reject(`**Hey, task \`${id}\` is already in my memory.**\n` +
                'Nothing changed. Do we have an update command yet?');
            } else {
              brain_data.taskoftheday[id] = setters;
              msg.send(`Task ${id} added.`);
            }
          }
        })
        ;
      }
    }
  });

  /**
   * remove task (ID)
   * You can use `list tasks` to get task IDs for removal.
   */
  robot.respond(/remove task [\d|a-z|A-Z]+/i, function(msg) {
    if (admin_room && admin_room != msg.message.room) {
      msg.send("You can't remove tasks here.");
      return;
    }
    robot.brain.setAutoSave(true);
    let brain_data = robot.brain.data;
    helpers.brainKeyExists('taskoftheday');

    let input_message = msg.match[0];
    let tasks = input_message.split('remove task ');
    for (let key in tasks) {
      if (key > 0) {
        // skip first
        let id = tasks[key];
        if (brain_data.taskoftheday[id]) {
          delete brain_data.taskoftheday[id];
          msg.send('task removed.');
        } else {
          msg.send(`Could not find task ${id} to remove.`);
        }
      }
    }
  });

  /**
   * list tasks
   * List the tasks currently in the robot brain into the room.
   * IDs are listed so that you can remove tasks if you need to.
   */
  robot.respond(/list tasks/i, function(msg) {
    if (admin_room && admin_room != msg.message.room) {
      msg.send("You can't list tasks here.");
      return;
    }
    let brain_data = robot.brain.data;
    helpers.brainKeyExists('taskoftheday');
    const stacker = new MessageStacker('Tasks:\n');
    for (let key in brain_data.taskoftheday) {
      stacker.appendOrSend(`${key}: ${brain_data.taskoftheday[key].url} \n`);
    }
    stacker.finalize('', 'No tasks. Please add some!');
  });

  /**
   * post task
   * This adds the ability to post a task at any time.
   * It DOES NOT reset the cron job, so if you do it an hour before
   *  the task is scheduled, it will post a new task on schedule.
   *
   * This is mostly for testing.
   */
  robot.respond(/post task/i, function(msg) {
    if (admin_room && admin_room != msg.message.room) {
      msg.send("You can't do that here.");
      return;
    }
    sendTask(robot);
  });
}

/**
 * Function to actually post a task.
 * Needed so I can ask on a schedule
 *  and also on command without duplicate code.
 * @param  robot
 */
function sendTask(robot) {
  let brain_data = robot.brain.data;

  // Loop through and restructure the totd brain object into
  // an array of keys, so I can more easily pick a random one
  let list = [];
  let last_posted_date = '';
  for (let key in brain_data.taskoftheday) {
    last_posted_date = brain_data.taskoftheday[key].last_posted_date;
    if (last_posted_date) {
      let date = new Date(last_posted_date);
      let d = new Date();
      d.setDate(d.getDate() - 8);
      if (d > date) {
        // Add task to array of possible tasks to choose from
        list.push(key);
      }
    } else {
      // Add task to array of possible tasks to choose from
      list.push(key);
    }
  }
  if (list.length == 0) {
    robot.messageRoom(admin_room, 'No tasks available.');
  } else {
    // Pick a random key
    let item = list[Math.floor(Math.random() * list.length)];
    // Choose the random task
    let task = brain_data.taskoftheday[item];

    // Record last posted date
    // https://stackoverflow.com/a/4929629/6078296
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); // January is 0!
    let yyyy = today.getFullYear();
    today = `${yyyy}-${mm}-${dd}`;
    task.last_posted_date = today;

    // Actually post the task
    const embedMessage = new MessageEmbed()
      .setColor('#c4000d')
      .setAuthor('Task of the Day')
      .setTitle(task.url)
      .setURL(task.url)
      .setDescription((task.title ? `**${task.title}**\r\n` : '') +
        `"${task.text}"\n *${task.lines} times* `)
      .setFooter({ text: `Submitted by ${task.submitted_by}. \r\nIf you would like to add your task(s) to our task-of-the-day, please reach out to Mulv.` })
      ;
    if (task.notes) {
      embedMessage.addField('Notes', task.notes)
    }

    const channel = robot.client.channels.cache.get(totd_room);
    channel.send({ embeds: [embedMessage] });
  }
}
