const GUILD = process.env.GUILD_ID;

module.exports.reloadAllUsersInBrain = function (robot, msg) {
  robot.client.guilds.cache.get(GUILD).members.fetch()
    .then(members => {
      if (msg) {
        msg.send('Okay.');
      }
      let brain_users = robot.brain.data.users;

      members.forEach((user, key) => {
        let room = '';
        if (brain_users[user.id]) {
          room = brain_users[user.id].room;
        }
        let name = '';
        if (user.displayName) {
          name = user.displayName;
        } else {
          name = user.user.username;
        }
        brain_users[user.id] = {
          'id': user.id,
          'name': name,
          'nickname': user.displayName,
          'original_username': user.user.username,
          'discriminator': user.user.discriminator,
          'room': room,
          'bot': user.user.bot
        };
      });
    }).then(complete => {
      if (msg) {
        msg.send('User list has been updated.');
      }
    });
}
