
const helpers = require('../src/helpers.js');

module.exports.brainConsentUser = function (sender) {
  helpers.brainKeyExists('consent', sender.id);
}

module.exports.hasConsent = function (top_user, sub_user) {
  // Make sure the sub has a consent record
  let consent_list = helpers.brainKeyExists('consent', sub_user.id);
  if (consent_list.safeword) {
    return false;
  }
  if (consent_list.everyone) {
    return true;
  }
  if (consent_list[top_user.id]) {
    return true;
  }
  return false;
}

/**
 * Same as hasConsent() except it takes IDs instead of objects
 * @param  int top_user_id
 * @param  int sub_user_id
 * @return boolean
 */
module.exports.hasConsentByID = function (top_user_id, sub_user_id) {
  // Make sure the sub has a consent record
  let consent_list = helpers.brainKeyExists('consent', sub_user_id);
  if (consent_list.safeword) {
    return false;
  }
  if (consent_list.everyone) {
    return true;
  }
  if (consent_list[top_user_id]) {
    return true;
  }
  return false;
}
