/* global robot */

/**
 * Message Stacker
 * Use this when looping to build a message so that it can
 *  stack it up and break it appart appropriately.
 */
module.exports = class MessageStacker {
  constructor(prepend) {
    this.prepend = prepend;
    this.message_cache = '';
  }

  appendOrSend(message_temp) {
    // Discord character limit of 2000 characters
    if ((this.prepend + this.message_cache + message_temp).length > 2000) {
      robot.discordJsMessage.reply(this.prepend + this.message_cache);
      this.message_cache = message_temp;
      this.prepend = '';
    } else {
      this.message_cache += message_temp;
    }
  }

  finalize(append = '', no_message = '') {
    if ((this.prepend + this.message_cache).length === 0
      && no_message.length > 0
    ) {
      robot.discordJsMessage.reply(no_message);
    } else if ((this.prepend + this.message_cache).length === 0) {
      robot.discordJsMessage.react('🤷‍♀️');
    } else {
      if ((this.prepend + this.message_cache + append).length > 2000) {
        robot.discordJsMessage.reply(this.prepend);
        robot.discordJsMessage.reply(this.message_cache);
        robot.discordJsMessage.reply(append);
      } else {
        robot.discordJsMessage.reply(this.prepend + this.message_cache + append);
      }
    }
  }
}
