/* global robot */

const helpers = require('../helpers.js');
const consent = require('../consent.js');
const _ = require('underscore');

/**
 * Slowdown
 * Use this to get corner of shame slowdowns
 * @param  User user
 */
module.exports = class Slowdown {
  constructor(user) {
    this.user = user;
    this.count = 0;
    this.s = 0;
    this.setter_id = null;
  }

  /**
   * Gets total slowdown in seconds, including modifiers
   * @return int Seconds
   */
  getTotalSlowdownSeconds() {
    let modifier = getCountModifierSeconds(this.user);
    let setSeconds = getSlowRecordSeconds(this.user);
    return +modifier + +setSeconds;
  }

  /**
   * gets the slow count modifier in seconds
   * @return int modifer seconds
   */
  getCountModifierSeconds() {
    let modifier = 0;
    helpers.brainKeyExists('count_fail_opt_out');
    if (robot.brain.data.count_fail_opt_out[this.user.id]) {
      modifier = 0;
    } else {
      if (robot.brain.data.count_fail_tracking[this.user.id]) {
        let tracking = robot.brain.data.count_fail_tracking[this.user.id];
        if (tracking > 0) {
          modifier = +tracking - 1;
        }
      }
    }
    return modifier;
  }

  /**
   * Sifts though the set slowdown records and finds the highest one
   * If there are more than one that are highest, it returns the first one it found
   */
  findSlowdownRecord() {
    let slowdown = helpers.brainKeyExists('slowdown', this.user.id);
    if (_.isEmpty(slowdown)) {
      return false;
    } else {
      for (let setter_id in slowdown) {
        if (+this.s < +slowdown[setter_id].seconds) {
          // if this records is higher than current s
          // check for consent before updating
          if (consent.hasConsentByID(setter_id, this.user.id)
            || slowdown[setter_id].self // set yourself
          ) {
            // consent is there, or you set this yourself
            this.s = slowdown[setter_id].seconds;
            this.setter_id = setter_id;
          }
        }
        this.count++;
      }
    }
  }

  /**
   * Get the set seconds from the record
   * @return int seconds
   */
  getSlowRecordSeconds() {
    this.findSlowdownRecord();
    return this.s;
  }

  /**
   * Get the quantity of set records
   * @return int count of records
   */
  getSlowRecordCount() {
    this.findSlowdownRecord();
    return this.count;
  }

  /**
   * Get the setter user ID from the record
   * @return string User ID
   */
  getSlowRecordSetterId() {
    this.findSlowdownRecord();
    return this.setter_id;
  }
}
