/* global robot */

const helpers = require('../src/helpers.js');
const shame_role = process.env.SHAME_ROLE;

// RETURNS A PROMISE
// Because it has to look up the guild member
module.exports.userHasShameByID = async function(user_id) {
  let guildMember = await robot.client.guilds.cache.get(process.env.GUILD_ID).members.fetch(user_id);
  // return if the user has the shame role or not
  return guildMember.roles.cache.some(role => role.name === shame_role);
};


module.exports.shameUserRecord = function(user, number, timer, source) {
  let time_now = new Date().getTime();
  let over = '';
  if (timer) {
    over = time_now + timer;
  } else {
    // 24 hours in milliseconds
    over = time_now + 86400000;
  }

  helpers.brainKeyExists('count_fail_shame_list');
  robot.brain.data.count_fail_shame_list[user.id] = {
    'user_id': user.id,
    'number': number,
    'received': time_now,
    'timer_ms': timer,
    'over': over,
    'source': source
  };
  robot.brain.save();
}

module.exports.shameUpdate = function(user_id, timer) {
  let time_now = new Date().getTime();
  let over = time_now + timer;
  helpers.brainKeyExists('count_fail_shame_list');
  robot.brain.data.count_fail_shame_list[user_id].over = over;
  robot.brain.data.count_fail_shame_list[user_id].timer_ms = timer;
  robot.brain.save();
}

// Why ask for user ID and not user? Because this is more universal
// Roles require a GUILD MEMBER not USER,
// so if I ask for user_id I can always ensure I'm using the right object
module.exports.addShameByUserId = function(user_id) {
  robot.client.guilds.cache.get(process.env.GUILD_ID).members.fetch(user_id)
    .then(user => {
    // add the shame role to that user
      let role = user.guild.roles.cache.find(role => role.name === shame_role);
      user.roles.add(role);
    });
}
