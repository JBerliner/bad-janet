/* global robot */

// This could probably be written better.
// First, I don't think the .length actually does anything
// because these are objects, right? so length comes back like . . . null or some shit
// but it doesn't hurt anything I think, so fuck it. I'm leaving it
// at least until it annoys me
// or someone else decides to test and change it
//
// Second, is there a way to loop this to handle infinite keys? Probably.

module.exports.brainKeyExists = function (key, key2 = null, key3 = null) {
  // Make sure the consent key exists in the brain
  let brain_data = robot.brain.data;
  if (!brain_data[key] || brain_data[key].length == 0) {
    brain_data[key] = {};
  }
  if (!key2) {
    // If there's no other key, return brain with first key
    return brain_data[key];
  } else {
    // More keys, keep making things
    if (!brain_data[key][key2] || brain_data[key][key2].length == 0) {
      brain_data[key][key2] = {};
    }
    if (!key3) {
      // No more keys? Return this object
      return brain_data[key][key2];
    } else {
      // THIRD KEY!
      if (!brain_data[key][key2][key3] || brain_data[key][key2][key3].length == 0) {
        brain_data[key][key2][key3] = {};
      }
      // Return object with third key
      return brain_data[key][key2][key3];
    }
  }
}


/**
 * Get the first mentioned user ID when using robot.respond
 */
module.exports.getFirstUserIdFromMentionOnRespond = function (raw_message) {
  let mentions = raw_message.mentions;
  if (raw_message.guildId === null) {
    // In DMs there's only one mention! OR 0 mentions
    let mentioned_user = mentions.users.first(); // User object
    if (!mentioned_user) {
      // not "self" so now need to extract the ID
      let content = raw_message.content;
      let mention = content.match(/<@\d+>/i);
      if (mention) {
        return mention[0].match(/\d+/);
      }
    } else {
      return mentioned_user.id;
    }
  } else {
    // Only works in public messages, which is most commands
    let mentioned_user = mentions.users.first(2)[1]; // User object
    if (mentioned_user) {
      return mentioned_user.id;
    }
  }
}


/**
 * Get the first mentioned user ID when using robot.hear
 */
module.exports.getFirstUserIdFromMentionOnHear = function (raw_message) {
  let mentions = raw_message.mentions;
  let mentioned_user = mentions.users.first(); // User object
  if (!mentioned_user) {
    // Not "self" so now we need to extract the ID
    let content = raw_message.content;
    let mention = content.match(/<@\d+>/i);
    if (mention) {
      return mention[0].match(/\d+/);
    }
  } else {
    return mentioned_user.id;
  }
}
